package com.mengjingtech.easyrecorder;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.mengjingtech.easyrecorder.ShakeFileAdapter.LastItemShowListener;
import com.mengjingtech.easyrecorder.ShakeFileAdapter.ViewClickListener;
import com.mengjingtech.easyrecorder.ShakeFileManager.DataChangeListener;
import com.mengjingtech.easyrecorder.ShakePlayerService.PlayStateListener;
import com.mengjingtech.lib.CustomDialog;

public abstract class ShakeRecordListActivity extends BaseActivity {
	private static final boolean DEBUG = ShakeConst.DEBUG;
	private static final String TAG = "ShakeRecordListActivity";

	private int mPlayingPosition = -1;
	protected ShakeFileManager mShakeFileManager;
	private Dialog fileDeleteDialog, playDialog;
	protected ProgressDialog pDialog;

	public ArrayList<ShakeFile> mList;
	public ShakeFileAdapter mAdaptar;
	public ListView mListView;
	public int mType;
	protected ArrayList<ShakeFile> selectedItems = new ArrayList<ShakeFile>();
	protected int offset = 0;
	protected int limit = 10;
	protected SharedPreferences sp;
	protected boolean mPlayerServiceBound = false;
	protected ShakePlayerService playerService;
	LocalBroadcastManager lbm;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		sp = PreferenceManager.getDefaultSharedPreferences(getApplication());

		setContentView(R.layout.record_list);

		mShakeFileManager = ShakeFileManager.getInstance(getApplication());

		mListView = (ListView) findViewById(R.id.list);
		mList = new ArrayList<ShakeFile>();
		mAdaptar = new ShakeFileAdapter(getApplication(), mShakeFileManager);

		mListView.setAdapter(mAdaptar);

		startPlayerService();

		lbm = LocalBroadcastManager.getInstance(this);
		IntentFilter localFilter = new IntentFilter();
		localFilter.addAction(ShakeConst.ACTION_SYNC_FILE_DB_DONE);
		localFilter.addAction(ShakeConst.ACTION_SYNC_FILE_DB_BATCH);
		lbm.registerReceiver(mLocalReceiver, localFilter);

		IntentFilter ifilter = new IntentFilter();
		ifilter.addAction(Intent.ACTION_TIME_TICK);
		registerReceiver(mReceiver, ifilter);

		mShakeFileManager.registerDataChangeListener(mDataChangeListener);
		mAdaptar.setOnViewClickListener(mViewClickListener);
		mAdaptar.setLastItemShowListener(mLastItemShowListener);

		if (!mPlayerServiceBound) {
			bindToPlayerService();
		}

		loadAdView();
	}

	private void startPlayerService() {
		if (DEBUG)
			Log.d(TAG, "startPlayerService");
		Intent i = new Intent(this, ShakePlayerService.class);
		startService(i);
	}

	private void bindToPlayerService() {
		if (DEBUG)
			Log.d(TAG, "bindToPlayerService");
		Intent intent = new Intent(
				"com.mengjingtech.easyrecorder.PLAYER_SERVICE");
		intent.setPackage("com.mengjingtech.easyrecorder");
		bindService(intent, mPlayerServiceConnection, BIND_AUTO_CREATE);
	}

	@Override
	protected void onDestroy() {
		unLoadAdView();

		mHandler.removeCallbacks(updateThread);

		dismissProcessingDialog();
		unregisterReceiver(mReceiver);
		lbm.unregisterReceiver(mLocalReceiver);

		mShakeFileManager.unregisterDataChangeListener(mDataChangeListener);
		mAdaptar.setOnViewClickListener(null);
		mAdaptar.setLastItemShowListener(null);

		playerService.unregisterPlayStateListener(mPlayStateListener);

		if (mPlayerServiceBound) {
			unbindService(mPlayerServiceConnection);
			mPlayerServiceBound = false;
		}
		super.onDestroy();
	}

	private BroadcastReceiver mReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null) {
				String action = intent.getAction();
				if (Intent.ACTION_TIME_TICK.equals(action)) {
					Calendar cal = Calendar.getInstance();
					cal.setTimeInMillis(System.currentTimeMillis());
					int hour = cal.get(Calendar.HOUR_OF_DAY);
					int min = cal.get(Calendar.MINUTE);
					if (hour == 0 && min == 0) {
						onDateChanged();
					}
				}
			}
		}
	};

	private BroadcastReceiver mLocalReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null) {
				String action = intent.getAction();
				if (ShakeConst.ACTION_SYNC_FILE_DB_DONE.equals(action)
						|| ShakeConst.ACTION_SYNC_FILE_DB_BATCH.equals(action)) {
					onSyncFileDBDone();
				}
			}
			dismissProcessingDialog();
		}
	};

	protected void onSyncFileDBDone() {
		controller.loadMoreRecord(limit, offset);
	}

	protected void onDateChanged() {
		// Sub class should override this method to handle the date change
		// intent.
	}

	@Override
	protected void onResume() {
		if (DEBUG)
			Log.d(TAG, "onResume");
		super.onResume();
		// remove the notifications
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		String appName = getResources().getString(R.string.app_name);
		notificationManager.cancel(appName, R.id.notify_id);

		if (playerService != null
				&& (playerService.isPlaying() || playerService.isPaused())) {
			mHandler.removeCallbacks(updateThread);
			mHandler.post(updateThread);
		}
	}

	@Override
	protected void onPause() {
		if (DEBUG)
			Log.d(TAG, "onPause");
		mHandler.removeCallbacks(updateThread);
		super.onPause();
	}

	private ViewClickListener mViewClickListener = new ViewClickListener() {
		public void onViewClick(View view) {
			int position = (Integer) view.getTag(R.id.position);
			ShakeFile sf = mList.get(position);
			if (sf.isHead()) {
				// no use currently
				sf.toggleCollapsed();
				mAdaptar.notifyDataSetChanged();
			} else if (view.getId() == R.id.shakefile_item) {
				if (selectedItems.size() > 0) {
					if (toggleSelect(sf)) {
						invalidateOptionsMenu();
					}
					mAdaptar.notifyDataSetChanged();
				} else {
					showPlayDialog(sf);
				}
			} else if (view.getId() == R.id.img_play_stop) {
				// handle the play button is clicked.
				if (DEBUG)
					Log.d(TAG, "onViewClick mPlayingPosition = "
							+ mPlayingPosition);
				if (mPlayingPosition != -1) {
					// some file is in playing
					ShakeFile shakeFile = mList.get(mPlayingPosition);
					if (mPlayingPosition == position) {
						// select current active item.
						if (shakeFile.getState() == ShakeFile.STATE_PAUSED) {
							// current file has been paused, resume it.
							playerService.resume();
						} else {
							// current file is playing, pause it.
							playerService.pause();
						}
					} else {
						// select other items other than current active one,
						// should
						// stop current playing
						// and start the new playing.
						playerService.stop();
						startPlayback(position);
					}
				} else {
					// no active item now.
					startPlayback(position);
				}
				mAdaptar.notifyDataSetChanged();
			} else if (view.getId() == R.id.star_button) {
				sf.setStar(!sf.isStar());
				ShakeFileManager.getInstance(getApplication()).updateFile(sf);
			}
		}

		// return if the selected status changed.
		private boolean toggleSelect(ShakeFile sf) {
			boolean selecteStateChange = false;
			if (sf.isSelected()) {
				sf.setSelected(false);
				int size = selectedItems.size();
				for (int i = 0; i < size; i++) {
					if (selectedItems.get(i) == sf) {
						selectedItems.remove(i);
						break;
					}
				}
				if (selectedItems.size() == 0) {
					selecteStateChange = true;
				}
			} else {
				sf.setSelected(true);
				selectedItems.add(sf);
				if (selectedItems.size() == 1) {
					selecteStateChange = true;
				}
			}
			return selecteStateChange;
		}

		@Override
		public void onViewLongClick(View view) {
			// handle the list item view is long clicked.
			if (view.getId() == R.id.shakefile_item) {
				int position = (Integer) view.getTag(R.id.position);
				ShakeFile sf = mList.get(position);
				if (toggleSelect(sf)) {
					invalidateOptionsMenu();
				}

				mAdaptar.notifyDataSetChanged();
			}
		}
	};

	private void startPlayback(int position) {
		mPlayingPosition = position;
		ShakeFile shakeRecordFile = mList.get(position);
		// update the shakeFile state to read.
		if (shakeRecordFile.isUnread()) {
			shakeRecordFile.setUnread(false);
			// mShakeFileManager.updateFile(shakeRecordFile);
		}

		playerService.start(shakeRecordFile,
				ShakePlayerService.AUDIO_SOURCE_LIST_ACTIVITY);
		mAdaptar.notifyDataSetChanged();
	}

	private PlayStateListener mPlayStateListener = new PlayStateListener() {
		@Override
		public void onComplete() {
			if (DEBUG)
				Log.d(TAG, "onCompletion mPlayingPosition = "
						+ mPlayingPosition);
			mHandler.removeCallbacks(updateThread);
			mPlayingPosition = -1;
			mAdaptar.notifyDataSetChanged();
		}

		@Override
		public void onStop() {
			mHandler.removeCallbacks(updateThread);
			mPlayingPosition = -1;
			mAdaptar.setProgress(0);
			mAdaptar.notifyDataSetChanged();
		}

		@Override
		public void onReset() {
		}

		@Override
		public void onPaused() {
			mHandler.removeCallbacks(updateThread);
			mAdaptar.notifyDataSetChanged();
		}

		@Override
		public void onStartPlay() {
			mHandler.removeCallbacks(updateThread);
			mHandler.post(updateThread);
			mAdaptar.notifyDataSetChanged();
		}
	};

	final Handler mHandler = new Handler();

	Runnable updateThread = new Runnable() {
		public void run() {
			mAdaptar.setMax(playerService.getDuration());
			mAdaptar.setProgress(playerService.getCurrentPosition());
			if (DEBUG)
				Log.d(TAG,
						"updateThread duration = "
								+ playerService.getDuration()
								+ " current position = "
								+ playerService.getCurrentPosition());
			mHandler.postDelayed(updateThread, 1000);
			mAdaptar.notifyDataSetChanged();
		}
	};

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.clear();
		if (selectedItems.size() > 0) {
			getMenuInflater().inflate(R.menu.menu_selected_record, menu);
		} else {
			getMenuInflater().inflate(R.menu.menu_record, menu);

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
				SearchView searchView = (SearchView) menu.findItem(R.id.search)
						.getActionView();
				searchView
						.setSearchableInfo(searchManager
								.getSearchableInfo(new ComponentName(
										getPackageName(),
										"com.mengjingtech.easyrecorder.ShakeRecordSearchResultActivity")));
			}
		}
		return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent = new Intent();
		switch (item.getItemId()) {
		case R.id.delete:
			showFileDeleteDialog();
			break;
		case R.id.settings:
			intent.setClass(this, SettingsActivity.class);
			startActivity(intent);
			break;
		case R.id.share:
			int size = selectedItems.size();
			ArrayList<Uri> uris = new ArrayList<Uri>();
			for (int i = 0; i < size; i++) {
				File f = selectedItems.get(i).getFile();
				Uri u = Uri.fromFile(f);
				uris.add(u);
			}
			intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
			intent.setType("audio/*");
			intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(Intent.createChooser(intent, getResources()
					.getString(R.string.share)));
			break;
		case R.id.recommend:
			Resources res = getResources();
			String shareString = res.getString(R.string.share_text);
			intent.setAction(Intent.ACTION_SEND);
			intent.putExtra(Intent.EXTRA_TEXT, shareString);
			intent.setType("text/plain");
			startActivity(Intent.createChooser(intent, getResources()
					.getString(R.string.share)));
			break;
		case R.id.about:
			intent.setClass(this, ShakeAboutActivity.class);
			startActivity(intent);
			break;
		case R.id.show_star:
			intent.setClass(ShakeRecordListActivity.this,
					ShakeRecordListStarActivity.class);
			startActivity(intent);
			break;
		case R.id.recorder:
			intent.setClass(ShakeRecordListActivity.this,
					ShakeRecordActivity.class);
			startActivity(intent);
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private OnClickListener mFileDeleteClickListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			switch (arg0.getId()) {
			case R.id.button_cancel:
				break;
			case R.id.button_ok:
				stopPlaying(selectedItems);
				if (selectedItems.contains(playerService.getPlayFile())) {
					playerService.removeCurrentFile();
				}
				mShakeFileManager.deleteFiles(mList, selectedItems);
				selectedItems.clear();
				invalidateOptionsMenu();
				break;
			default:
				break;
			}
			fileDeleteDialog.dismiss();
		}
	};

	private void showPlayDialog(ShakeFile sf) {
		playDialog = new PlayDialog(this, playerService, sf);
		playDialog.show();
	}

	private void stopPlaying(ArrayList<ShakeFile> items) {
		// if there is any playing item in the pending delete list. release the
		// mediaplayer first.
		for (ShakeFile sf : items) {
			if (sf.getState() == ShakeFile.STATE_PLAYING
					|| sf.getState() == ShakeFile.STATE_PAUSED) {
				mHandler.removeCallbacks(updateThread);
				break;
			}
		}
	}

	private void showFileDeleteDialog() {
		View dialogView = View.inflate(this, R.layout.filelist_delete_dialog,
				null);
		fileDeleteDialog = new CustomDialog(this, dialogView,
				R.style.mengjingtech_dialog);

		TextView dialog_button_ok = (TextView) dialogView
				.findViewById(R.id.button_ok);
		dialog_button_ok.setClickable(true);
		dialog_button_ok.setOnClickListener(mFileDeleteClickListener);
		dialog_button_ok.setEnabled(true);

		TextView dialog_button_cancel = (TextView) dialogView
				.findViewById(R.id.button_cancel);
		dialog_button_cancel.setClickable(true);
		dialog_button_cancel.setOnClickListener(mFileDeleteClickListener);
		dialog_button_cancel.setEnabled(true);

		fileDeleteDialog.show();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && selectedItems.size() > 0) {
			for (ShakeFile sf : selectedItems) {
				sf.setSelected(false);
			}
			mAdaptar.notifyDataSetChanged();
			selectedItems.clear();
			invalidateOptionsMenu();
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	protected void showProcessingDialog(boolean cancelable) {
		String processing = getResources().getString(R.string.processing);
		// show progress dialog
		pDialog = new ProgressDialog(this);
		pDialog.setMessage(processing);
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(cancelable);
		pDialog.show();
	}

	protected void dismissProcessingDialog() {
		if (pDialog != null) {
			pDialog.dismiss();
		}
	}

	// could be overrided by subclass to do something when last item of list is
	// shown.
	protected void lastItemShow() {
	}

	private LastItemShowListener mLastItemShowListener = new LastItemShowListener() {
		@Override
		public void onLastItemShow() {
			lastItemShow();
		}
	};

	protected abstract void OnRecordChange();

	private DataChangeListener mDataChangeListener = new DataChangeListener() {
		@Override
		public void onDataChanged() {
			if (DEBUG)
				Log.d(TAG, "onDataChanged");
			OnRecordChange();
			selectedItems.clear();
			invalidateOptionsMenu();
		}
	};

	private ServiceConnection mPlayerServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			playerService = null;
			mPlayerServiceBound = false;
			playerService.unregisterPlayStateListener(mPlayStateListener);
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder binder) {
			mPlayerServiceBound = true;
			playerService = ((ShakePlayerService.PlayerBinder) binder)
					.getService();
			playerService.registerPlayStateListener(mPlayStateListener);
		}
	};

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onStop() {

		super.onStop();
	}
}
