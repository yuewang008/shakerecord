package com.mengjingtech.easyrecorder;

import java.lang.ref.WeakReference;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.ScaleAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.mengjingtech.easyrecorder.ShakePlayerService.PlayStateListener;
import com.mengjingtech.easyrecorder.ShakeService.RecordState;
import com.mengjingtech.easyrecorder.ShakeService.ShakeServiceBinder;
import com.mengjingtech.lib.CustomDialog;
import com.mengjingtech.lib.Util;

public class ShakeRecordActivity extends BaseActivity implements
		View.OnClickListener, OnTouchListener {
	private static final boolean DEBUG = ShakeConst.DEBUG;
	private static final String TAG = "ShakeRecordActivity";

	public enum State {
		CREATED, FOREGROUND, BACKGROUND, DESTROYED
	}

	public static final String IS_PLAYING = "is_playing";

	private ImageView mRecordButton, mDeleteButton, mRecordButtonBg,
			mStopRecordButton, mStarButton, mPlayButton, mStopPlayButton,
			mListButton;
	private TextView mRecordHint, tvRecordedFile, tvFileTime,
			tvCurrentPosition;
	private View vRecordedItem;
	private SeekBar seekbar;

	private static final long NO_USER_ACTIVITY_FINISH_DELAY = 15 * 1000;
	private static final int BUTTON_WIDTH = 140;
	private static final int BUTTON_HEIGHT = 140;
	private static final int VOLUME_FACTOR = 2;
	private static final long VOLUME_INTERVAL = 50;

	private Dialog fileSaveDialog, fileDeleteDialog;
	private EditText dialogFileNameEdit;
	private float mDensity;
	private float mWidth = BUTTON_WIDTH, mHeight = BUTTON_HEIGHT;
	private Typeface mTypeface;
	private boolean mBound = false;
	private boolean mPlayerServiceBound = false;
	private ShakeService mService;
	private Messenger mServiceMessenger;
	private boolean mUserActivity = false;

	private RecorderHandler mHandler = null;
	private Messenger mMessenger = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mHandler = new RecorderHandler(this);
		mMessenger = new Messenger(mHandler);

		Window win = getWindow();
		WindowManager.LayoutParams param = win.getAttributes();
		param.flags |= (WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
				| WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON
				| WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
		win.setAttributes(param);

		setContentView(R.layout.record);

		mRecordButton = (ImageView) findViewById(R.id.recordButton);
		mRecordButton.setOnClickListener(this);
		mRecordButton.setClickable(true);
		mRecordButtonBg = (ImageView) findViewById(R.id.recordButtonbg);

		mListButton = (ImageView) findViewById(R.id.list_button);
		mListButton.setOnClickListener(this);
		mListButton.setOnTouchListener(this);
		mListButton.setClickable(true);

		mStopRecordButton = (ImageView) findViewById(R.id.stop_record_button);
		mStopRecordButton.setOnClickListener(this);
		mStopRecordButton.setClickable(true);
		boolean leftHand = PreferenceManager.getDefaultSharedPreferences(this)
				.getBoolean(ShakeConst.PER_KEY_LEFT_HAND, false);
		if (leftHand) {
			RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mStopRecordButton
					.getLayoutParams();
			lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
			mStopRecordButton.setLayoutParams(lp);

			lp = (RelativeLayout.LayoutParams) mListButton.getLayoutParams();
			lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
			mListButton.setLayoutParams(lp);
		}

		mRecordHint = (TextView) findViewById(R.id.recordHint);
		mRecordHint.setText(Util.getStandardTime(0L));
		// mTypeface = Typeface.createFromAsset(getAssets(),
		// "fonts/constantia.ttf");
		mRecordHint.setTypeface(mTypeface);
		mRecordHint.setVisibility(View.VISIBLE);

		mDeleteButton = (ImageView) findViewById(R.id.delete_button);
		mDeleteButton.setOnClickListener(this);
		mDeleteButton.setClickable(true);

		mPlayButton = (ImageView) findViewById(R.id.play_button);
		mPlayButton.setOnClickListener(this);
		mPlayButton.setClickable(true);

		mStopPlayButton = (ImageView) findViewById(R.id.stop_play_button);
		mStopPlayButton.setOnClickListener(this);
		mStopPlayButton.setClickable(true);

		mStarButton = (ImageView) findViewById(R.id.star_button);
		mStarButton.setOnClickListener(this);
		mStarButton.setClickable(true);

		vRecordedItem = findViewById(R.id.recorded_item);
		tvRecordedFile = (TextView) findViewById(R.id.text_file);
		tvFileTime = (TextView) findViewById(R.id.text_duration);
		tvCurrentPosition = (TextView) findViewById(R.id.text_current_position);

		seekbar = (SeekBar) findViewById(R.id.seek_bar);
		seekbar.setOnSeekBarChangeListener(mOnSeekBarChangeListener);

		DisplayMetrics dm = getResources().getDisplayMetrics();
		mDensity = dm.density;

		initDialogs();

		loadAdView();

		mHandler.postDelayed(mNoUserActivityFinishRunnable,
				NO_USER_ACTIVITY_FINISH_DELAY);

		// notify the record service current record activity is in forground.
		// Bind to LocalService
		Intent intent = new Intent(this, ShakeService.class);
		intent.putExtra(ShakeService.BIND_FROM,
				ShakeService.BIND_FROM_RECORD_ACTIVITY);
		bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);

		startPlayerService();
		if (DEBUG)
			Log.d(TAG, "bindToPlayerService");
		intent = new Intent("com.mengjingtech.easyrecorder.PLAYER_SERVICE");
		intent.setPackage("com.mengjingtech.easyrecorder");
		bindService(intent, mPlayerServiceConnection, BIND_AUTO_CREATE);

		if (DEBUG)
			Log.d(TAG, "onCreate");
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onStop() {
		if (DEBUG)
			Log.d(TAG, "onStop");
		super.onStop();
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (mBound && mService != null) {
			mService.setRecordActivityState(State.FOREGROUND);
		}

		if (mBound) {
			mService.cancelLoadingToast();
		}
	}

	@Override
	protected void onPause() {
		if (mBound && mService != null) {
			mService.setRecordActivityState(State.BACKGROUND);
		}
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		unLoadAdView();

		// instance = null;
		mHandler.removeCallbacksAndMessages(null);
		mHandler = null;
		mMessenger = null;

		// if (vRecordedItem.getVisibility() == View.VISIBLE) {
		// stopPlayback();
		// }

		if (mService != null) {
			mService.setRecordActivityState(State.DESTROYED);
		}
		// Unbind from the service
		if (mBound) {
			unbindService(mServiceConnection);
			mBound = false;
		}

		if (mPlayerServiceBound) {
			playerService.unregisterPlayStateListener(mPlayStateListener);
			unbindService(mPlayerServiceConnection);
			mPlayerServiceBound = false;
		}

		if (DEBUG)
			Log.d(TAG, "onDestroy");

		super.onDestroy();
	}

	private void startPlayerService() {
		if (DEBUG)
			Log.d(TAG, "startPlayerService");
		Intent i = new Intent(getApplicationContext(), ShakePlayerService.class);
		startService(i);
	}

	private void initRecordState() {
		if (mService.isRecording()) {
			processStartRecording();
		}
	}

	// show the playing UI if there is a playing media from this activity.
	private void initPlayingState() {
		if (mPlayerServiceBound && playerService != null) {
			boolean isPlaying = playerService.isPlaying();
			boolean isPaused = playerService.isPaused();
			boolean isPrepared = playerService.isPrepared();
			if ((isPlaying || isPaused || isPrepared)
					&& playerService.getAudioSourceType() == ShakePlayerService.AUDIO_SOURCE_RECORD_ACTIVITY) {
				showPlayUI();
			}
		}
	}

	private ServiceConnection mServiceConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mBound = true;
			if (service != null) {
				ShakeServiceBinder binder = (ShakeServiceBinder) service;
				mService = binder.getService();
				mServiceMessenger = binder.getMessenger();
				initRecordState();
				mService.cancelLoadingToast();
				mService.setRecordActivityState(State.FOREGROUND);
			}

			if (mServiceMessenger != null) {
				try {
					Message msg = Message.obtain(null,
							ShakeConst.ServiceMessage.ACTIVITY_MESSENGER);
					msg.replyTo = mMessenger;
					mServiceMessenger.send(msg);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName className) {
			mBound = false;
			mService = null;
			mServiceMessenger = null;
		}
	};

	private void processStartRecording() {
		mHandler.removeCallbacks(mVolumeRunnable);
		mHandler.postDelayed(mVolumeRunnable, VOLUME_INTERVAL);
		mRecordHint.setTextColor(Color.RED);
		mRecordButton.setImageResource(R.drawable.pause_circle_red_button);
		mStopRecordButton.setVisibility(View.VISIBLE);

		hidePlayUI();

		if (playerService != null
				&& (playerService.isPlaying() || playerService.isPaused())) {
			stopPlayback();
		}
	}

	private void processEndRecording() {
		mRecordHint.setText(Util.getStandardTime(0L));
		mRecordHint.setTextColor(Color.GRAY);
		mHandler.removeCallbacks(mVolumeRunnable);
		controller.stopRecording(mService);
		showProcessingDialog(true);
	}

	@Override
	public void onClick(View arg0) {
		mUserActivity = true;
		switch (arg0.getId()) {
		case R.id.recordButton:
			if (mService != null) {
				if (mService.getRecordState() == RecordState.STOPPED
						|| mService.getRecordState() == RecordState.PAUSED) {
					// start recording here.
					mService.startRecording();
					processStartRecording();
				} else {
					mService.pauseRecording();
					mRecordButton
							.setImageResource(R.drawable.record_light_blue_button);
				}
			}
			break;
		case R.id.delete_button:
			showFileDeleteDialog();
			break;
		case R.id.stop_record_button:
			processEndRecording();
			break;
		case R.id.play_button:
			if (playerService.isPlaying()) {
				pausePlayback();
			} else if (playerService.isPaused()) {
				resumePlayback();
			} else {
				startPlayback(seekbar.getProgress());
			}
			break;
		case R.id.stop_play_button:
			if (playerService != null
					&& (playerService.isPlaying() || playerService.isPaused())) {
				stopPlayback();
				playerService.preparePlayback(mService.getRecordFile());
			}
			break;
		case R.id.star_button:
			if (mService != null) {
				mService.getRecordFile().setStar(
						!mService.getRecordFile().isStar());
				ShakeFileManager.getInstance(getApplicationContext())
						.updateFileWithName(mService.getRecordFile());
				int imageRes = mService.getRecordFile().isStar() ? R.drawable.ic_btn_star_on
						: R.drawable.ic_btn_star_off;
				mStarButton.setImageResource(imageRes);
			}
			break;
		case R.id.list_button:
			// launch the record list activity.
			Intent intent = new Intent(this, ShakeRecordListFullActivity.class);
			startActivity(intent);
			finish();
			break;
		default:
			break;
		}
	}

	private OnClickListener mFileDeleteClickListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			switch (arg0.getId()) {
			case R.id.button_cancel:
				break;
			case R.id.button_ok:
				stopPlayback();
				ShakeFileManager.getInstance(getApplicationContext())
						.deleteFile(mService.getRecordFile());
				hidePlayUI();
				if (mPlayerServiceBound && playerService != null) {
					playerService.removeCurrentFile();
				}
				break;
			default:
				break;
			}
			fileDeleteDialog.dismiss();
		}
	};

	private OnClickListener mFileSaveClickListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			switch (arg0.getId()) {
			case R.id.button_cancel:
				if (mService != null) {
					mService.getRecordFile().delete();
				}
				break;
			case R.id.button_save:
				String fileName = dialogFileNameEdit.getText().toString()
						.trim()
						+ ShakeConst.SUFFIX;
				if (mService != null
						&& mService.getRecordFile().moveFile(
								Util.appSaveDir + fileName)) {
					if (ShakeFileManager.getInstance(getApplicationContext())
							.isFileExist(mService.getRecordFile())) {
						// file already existed. ask user to change file name.
						toast(R.string.file_exist);
						return;
					} else {
						ShakeFileManager.getInstance(getApplicationContext())
								.addFile(mService.getRecordFile());
					}
					showPlayUI();
				}
				break;
			default:
				break;
			}
			fileSaveDialog.dismiss();
			mStopRecordButton.setVisibility(View.INVISIBLE);
		}
	};

	private OnSeekBarChangeListener mOnSeekBarChangeListener = new OnSeekBarChangeListener() {

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			if (fromUser) {
				playerService.seekTo(progress);
			}
			tvCurrentPosition.setText(Util.getStandardTime(progress / 1000));
		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {

		}

	};

	private void stopPlayback() {
		if (playerService != null) {
			playerService.stop();
		}
	}

	private void resumePlayback() {
		if (playerService != null && playerService.getPlayFile() != null) {
			playerService.resume();
		}
	}

	private void pausePlayback() {
		if (playerService != null && playerService.getPlayFile() != null) {
			playerService.pause();
		}
	}

	private void startPlayback(int progress) {
		if (mService == null) {
			return;
		}

		ShakeFile sf = mService.getRecordFile();
		if (sf.isUnread()) {
			sf.setUnread(false);
			ShakeFileManager.getInstance(getApplication()).updateFile(sf);
		}

		if (playerService != null && playerService.getPlayFile() != null) {
			playerService.registerPlayStateListener(mPlayStateListener);
			try {
				playerService.start(sf,
						ShakePlayerService.AUDIO_SOURCE_RECORD_ACTIVITY,
						progress);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			}
		}
	}

	private void showPlayUI() {
		if (mBound && mService != null) {
			tvRecordedFile
					.setText(mService
							.getRecordFile()
							.getFileName()
							.substring(
									0,
									mService.getRecordFile().getFileName()
											.length() - 4));
		}
		playerService.preparePlayback(mService.getRecordFile());
		seekbar.setMax(playerService.getDuration());
		seekbar.setProgress(0);
		tvFileTime
				.setText(Util.getStandardTime(playerService.getDuration() / 1000));
		vRecordedItem.setVisibility(View.VISIBLE);
	}

	private void hidePlayUI() {
		vRecordedItem.setVisibility(View.GONE);
		mHandler.removeCallbacks(updateThread);
	}

	private void showFileDeleteDialog() {
		fileDeleteDialog.show();
	}

	private void showFileSaveDialog() {
		if (mService != null) {
			String filename = mService.getRecordFile().getFileName().toString();
			dialogFileNameEdit.setText(filename.substring(0,
					filename.length() - 4));
		}
		fileSaveDialog.show();
	}

	private void initDialogs() {
		// init the save dialog.
		View dialogView = View.inflate(this, R.layout.file_save_dialog, null);
		fileSaveDialog = new CustomDialog(this, dialogView,
				R.style.mengjingtech_dialog);

		TextView fileName = (TextView) dialogView
				.findViewById(R.id.textViewFileName);
		fileName.setTypeface(mTypeface);

		TextView dialog_button_save = (TextView) dialogView
				.findViewById(R.id.button_save);
		dialog_button_save.setTypeface(mTypeface, Typeface.BOLD);
		dialog_button_save.setOnClickListener(mFileSaveClickListener);

		TextView dialog_button_cancel = (TextView) dialogView
				.findViewById(R.id.button_cancel);
		dialog_button_cancel.setTypeface(mTypeface, Typeface.BOLD);
		dialog_button_cancel.setOnClickListener(mFileSaveClickListener);

		dialogFileNameEdit = (EditText) dialogView
				.findViewById(R.id.editTextFileName);

		// init the delete dialog.
		dialogView = View.inflate(this, R.layout.file_delete_dialog, null);
		fileDeleteDialog = new CustomDialog(this, dialogView,
				R.style.mengjingtech_dialog);

		TextView dialog_button_ok = (TextView) dialogView
				.findViewById(R.id.button_ok);
		dialog_button_ok.setTypeface(mTypeface, Typeface.BOLD);
		dialog_button_ok.setOnClickListener(mFileDeleteClickListener);

		dialog_button_cancel = (TextView) dialogView
				.findViewById(R.id.button_cancel);
		dialog_button_cancel.setTypeface(mTypeface, Typeface.BOLD);
		dialog_button_cancel.setOnClickListener(mFileDeleteClickListener);
	}

	static class RecorderHandler extends Handler {
		private WeakReference<ShakeRecordActivity> outer;

		public RecorderHandler(ShakeRecordActivity activity) {
			outer = new WeakReference<ShakeRecordActivity>(activity);
		}

		@Override
		public void handleMessage(Message msg) {
			ShakeRecordActivity activity = outer.get();
			switch (msg.what) {
			case ShakeConst.ServiceMessage.UPDATE_ACTIVITY_RECORD_TIME:
				activity.mRecordHint.setText(Util.getStandardTime(msg.arg1));
				break;
			case ShakeConst.ServiceMessage.FINISH_RECORD_ACTIVITY:
				activity.finish();
				break;
			}
		}
	};

	Runnable mNoUserActivityFinishRunnable = new Runnable() {
		public void run() {
			// if no user activity in a period of time, exit record activity.
			if (!mUserActivity) {
				finish();
			}
		}
	};

	Runnable mHapticRunnable = new Runnable() {
		public void run() {
			mRecordButton
					.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
		}
	};

	Runnable mVolumeRunnable = new Runnable() {
		public void run() {
			updateVolume();
		}
	};

	private void updateVolume() {
		if (mService == null
				|| mService.getRecordState() != RecordState.RECORDING) {
			return;
		}

		int volume = mService.getRecordVolume();
		int value = (volume + 1) * 6 / 2048;
		if (value > 100) {
			value = 100;
		}

		// to make volume smooth
		value = (int) (Math.sqrt((double) value) * 10);

		onVolumeChange(value);

		mHandler.postDelayed(mVolumeRunnable, VOLUME_INTERVAL);
	}

	AnimationListener mAnimationListener = new AnimationListener() {

		@Override
		public void onAnimationStart(Animation animation) {
		}

		@Override
		public void onAnimationEnd(Animation animation) {
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
		}

	};

	private void onVolumeChange(int value) {
		float width, height;

		width = height = (BUTTON_WIDTH + ((VOLUME_FACTOR * value) / mDensity));

		float widthRatio = width / BUTTON_WIDTH;
		float heightRatio = height / BUTTON_WIDTH;
		float oldWidthRatio = mWidth / BUTTON_WIDTH;
		float oldHeightRatio = mHeight / BUTTON_HEIGHT;
		Animation scaleAnimation = new ScaleAnimation(oldWidthRatio,
				widthRatio, oldHeightRatio, heightRatio,
				Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
				0.5f);
		scaleAnimation.setDuration(VOLUME_INTERVAL);
		mRecordButtonBg.startAnimation(scaleAnimation);
		mWidth = width;
		mHeight = height;
		// startAnimation();
	}

	Runnable updateThread = new Runnable() {
		public void run() {
			if (playerService.isPlaying()) {
				int position = playerService.getCurrentPosition();
				if (DEBUG)
					Log.d(TAG, "position = " + position);
				// seekbar.setProgress(playerService.getCurrentPosition());
				seekbar.setProgress(position);
				mHandler.postDelayed(updateThread,
						ShakeConst.SEEKBAR_UPDATE_INTERVAL);
			}
		}
	};

	public void handleMsg(android.os.Message msg) {
		switch (msg.what) {
		case Controller.STOP_RECORDING:
			if (msg.arg1 == 0) {
				showFileSaveDialog();
				mRecordButton
						.setImageResource(R.drawable.record_light_blue_button);
			}

			dismissProcessingDialog();
			break;
		}
	}

	private ShakePlayerService playerService;

	private ServiceConnection mPlayerServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			playerService = null;
			mPlayerServiceBound = false;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder binder) {
			mPlayerServiceBound = true;
			playerService = ((ShakePlayerService.PlayerBinder) binder)
					.getService();
			// initPlayingState();
		}
	};

	private PlayStateListener mPlayStateListener = new PlayStateListener() {
		@Override
		public void onComplete() {
			mPlayButton
					.setBackgroundResource(R.drawable.play_circle_light_blue_button);
			playerService.unregisterPlayStateListener(mPlayStateListener);
			seekbar.setProgress(0);
			playerService.preparePlayback(mService.getRecordFile());
		}

		@Override
		public void onStop() {
			mPlayButton
					.setBackgroundResource(R.drawable.play_circle_light_blue_button);
			mHandler.removeCallbacks(updateThread);
			seekbar.setProgress(0);
		}

		@Override
		public void onReset() {
		}

		@Override
		public void onPaused() {
			mPlayButton
					.setBackgroundResource(R.drawable.play_circle_light_blue_button);
			mHandler.removeCallbacks(updateThread);
		}

		@Override
		public void onStartPlay() {
			mPlayButton
					.setBackgroundResource(R.drawable.pause_circle_light_blue_button);
			mHandler.removeCallbacks(updateThread);
			mHandler.post(updateThread);
		}
	};

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (v.getId() == R.id.list_button) {
			int action = event.getAction();
			switch (action) {
			case MotionEvent.ACTION_DOWN:
				v.setBackgroundColor(Color.WHITE);
				break;
			case MotionEvent.ACTION_UP:
				v.setBackgroundResource(R.drawable.blue_circle);
				break;
			default:
			}
		}
		return false;
	}
}
