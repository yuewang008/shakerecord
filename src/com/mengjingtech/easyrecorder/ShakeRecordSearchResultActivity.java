package com.mengjingtech.easyrecorder;

import java.util.ArrayList;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;

import com.mengjingtech.lib.Util;
import com.mengjingtech.easyrecorder.R;

public class ShakeRecordSearchResultActivity extends ShakeRecordListActivity {
    private static final boolean DEBUG = ShakeConst.DEBUG;
    private static final String TAG = "ShakeRecordSearchResultActivity";
    private String mQuery;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mList.clear();
        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            // handles a search query
            String query = intent.getStringExtra(SearchManager.QUERY);
            mQuery = query;
            showResults();
        }
    }

    private void showResults() {
        controller.loadMoreRecord(limit, offset, mQuery);
        showProcessingDialog();
    }
    
    protected void onSyncFileDBDone() {
        super.onSyncFileDBDone();
        showResults();
    }
    
    protected void onDateChanged() {
        showResults();
    }
    
    protected void lastItemShow() {
        offset += limit;
        controller.loadMoreRecord(limit, offset, mQuery);
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.removeItem(R.id.show_star);
        menu.removeItem(R.id.search);
        return true;
    }
    
    public void handleMsg(android.os.Message msg) {
        switch (msg.what) {
        case Controller.LOAD_MORE_RECORD_QUERY:
            if (msg.arg1 == 0) {
                mList.addAll(((ArrayList<ShakeFile>) msg.obj));
                mAdaptar.setList(mList);
                mAdaptar.notifyDataSetChanged();
            }

            dismissProcessingDialog();
            break;
        }
    }

    @Override
    protected void OnRecordChange() {
        mList.clear();

        if (sp.getBoolean(ShakeConst.PER_SYNC_FILE_DB_DONE, false)) {
            controller.loadMoreRecord(offset+limit, 0, mQuery);
        }
    }
}
