package com.mengjingtech.easyrecorder;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mengjingtech.lib.Util;

public class ShakeNotiManager {
	private static final boolean DEBUG = ShakeConst.DEBUG;
	private static final String TAG = "ShakeNotiManager";

	private static ShakeNotiManager mShakeNotiManager = null;
	private Context mContext = null;
	private ArrayList<ShakeNoti> mList;
	private ShakeDBHelper mShakeDBHelper;

	public static ShakeNotiManager getInstance(Context context) {
		if (mShakeNotiManager == null) {
			mShakeNotiManager = new ShakeNotiManager(context);
		}
		return mShakeNotiManager;
	}

	private ShakeNotiManager(Context context) {
		mContext = context;
		mShakeDBHelper = new ShakeDBHelper(context, ShakeDBHelper.DBNAME, null, 1);
		mList = new ArrayList<ShakeNoti>();
		getNotiFromDB();
	}

	public int getSize() {
		return mList.size();
	}

	public ArrayList<ShakeNoti> getShakeNotiList() {
		return mList;
	}

	private ShakeNoti getById(int id) {
		if (DEBUG)
			Log.d(TAG, "getById id = " + id);
		for (ShakeNoti sn : mList) {
			if (sn.getId() == id) {
				return sn;
			}
		}

		return null;
	}

	private void getNotiFromDB() {
		Cursor cr = null;
		String sql = "SELECT * FROM " + ShakeDBHelper.TABLE_NOTI_NAME;
		SQLiteDatabase db = mShakeDBHelper.getWritableDatabase();
		cr = db.rawQuery(sql, null);
		if (cr.getCount() == 0) {
			mList.clear();
		}
		while (cr.moveToNext()) {
			int id = cr.getInt(cr.getColumnIndex(ShakeDBHelper.FIELD_ID));
			String notiTime = cr.getString(cr.getColumnIndex(ShakeDBHelper.FIELD_NOTI_TIME));
			boolean enable = cr.getInt(cr.getColumnIndex(ShakeDBHelper.FIELD_NOTI_ENABLE)) == 0 ? false
					: true;
			Date date = Util.formatStringToTime(notiTime);

			ShakeNoti sn = new ShakeNoti(id, date, enable);
			mList.add(sn);
		}
		cr.close();
		sort();
	}

	public void sort() {
		Collections.sort(mList, new Comparator<ShakeNoti>() {
			@Override
			public int compare(ShakeNoti lhs, ShakeNoti rhs) {
				Date timel = lhs.getDate();
				Date timer = rhs.getDate();
				return timel.compareTo(timer);
			}
		});
	}

	private void addItemToDB(ShakeNoti sn) {
		ContentValues cv = new ContentValues();
		cv.put("noti_time", Util.formatTimeToString(sn.getDate()));
		cv.put("noti_enable", String.valueOf(sn.isEnabled() ? 1 : 0));
		SQLiteDatabase db = mShakeDBHelper.getWritableDatabase();
		db.insertOrThrow(ShakeDBHelper.TABLE_NOTI_NAME, null, cv);
	}

	private void deleteItemFromDB(ShakeNoti sn) {
		ContentValues cv = new ContentValues();
		cv.put("noti_time", Util.formatTimeToString(sn.getDate()));
		cv.put("noti_enable", String.valueOf(sn.isEnabled() ? 1 : 0));
		SQLiteDatabase db = mShakeDBHelper.getWritableDatabase();
		db.delete(ShakeDBHelper.TABLE_NOTI_NAME, "id=?",
				new String[] { String.valueOf(sn.getId()) });
	}

	private void updateItemInDB(ShakeNoti sn) {
		ContentValues cv = new ContentValues();
		cv.put("noti_time", Util.formatTimeToString(sn.getDate()));
		cv.put("noti_enable", String.valueOf(sn.isEnabled() ? 1 : 0));
		SQLiteDatabase db = mShakeDBHelper.getWritableDatabase();
		db.update(ShakeDBHelper.TABLE_NOTI_NAME, cv, "id = ?",
				new String[] { String.valueOf(sn.getId()) });
	}

	public void updateNoti(ShakeNoti sn) {
		ShakeNoti foundSN = getById(sn.getId());
		if (foundSN != null) {
			foundSN.setDate(sn.getDate());
			foundSN.setEnabled(sn.isEnabled());
		}
		updateItemInDB(sn);
		sort();
		onNotiTimeChange();
	}

	public void addNoti(ShakeNoti sn) {
		mList.add(sn);
		addItemToDB(sn);
		sort();
		onNotiTimeChange();
	}

	public void deleteNoti(int index) {
		ShakeNoti sn = mList.get(index);
		deleteItemFromDB(sn);
		mList.remove(index);
		onNotiTimeChange();
	}

	public void deleteNotis(ArrayList<ShakeNoti> items) {
		if (items.size() <= 0) {
			return;
		}
		mList.removeAll(items);
		for (ShakeNoti sn : items) {
			deleteItemFromDB(sn);
		}
		onNotiTimeChange();
	}

	public ShakeNoti getNoti(int position) {
		return mList.get(position);
	}

	private void initShakeNotiList() {
		getNotiFromDB();
		// align all the noti date to same date.
		for (ShakeNoti shakeNoti : mList) {
			shakeNoti.setDate(clearDate(shakeNoti.getDate()));
		}

		// sort the alarm timer list.
		Collections.sort(mList, new Comparator<ShakeNoti>() {
			@Override
			public int compare(ShakeNoti lhs, ShakeNoti rhs) {
				Date timel = lhs.getDate();
				Date timer = rhs.getDate();

				if (timel.before(timer)) {
					return 1;
				} else if (timel.equals(timer)) {
					return 0;
				} else {
					return -1;
				}
			}
		});
	}

	// align the date to same year, month and day.
	private Date clearDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(1970, 0, 1);
		return calendar.getTime();
	}

	// get the next coming ShakeNoti.
	public Calendar getComingAlarm(Date date) {
		int nextDay = 0;
		if (DEBUG)
			Log.d(TAG, "getComingAlarm date = " + date.toString());
		Calendar comingAlarm = Calendar.getInstance();
		Calendar today = Calendar.getInstance();
		today.setTimeInMillis(System.currentTimeMillis());
		ShakeNoti comingNoti = null;
		Date clearedDate = clearDate(date);
		if (DEBUG)
			Log.d(TAG, "getComingAlarm clearDate = " + clearedDate.toString());
		if (DEBUG)
			Log.d(TAG, "clearedDate =  " + clearedDate);

		// find the coming alarm time.
		for (ShakeNoti sn : mList) {
			if (DEBUG)
				Log.d(TAG, "getComingAlarm sn = " + sn.toString());
			if (sn.isEnabled() && sn.getDate().compareTo(clearedDate) > 0) {
				comingNoti = sn;
				break;
			}
		}
		if (comingNoti == null) {
			comingNoti = mList.get(0);
			nextDay = 1;
		}

		// set the alarm to today.
		comingAlarm.setTime(comingNoti.getDate());
		comingAlarm.set(today.get(Calendar.YEAR), today.get(Calendar.MONTH),
				today.get(Calendar.DAY_OF_MONTH) + nextDay);
		if (DEBUG)
			Log.d(TAG, "getComingAlarm comingAlarm = "
					+ comingAlarm.getTime().toString());
		return comingAlarm;
	}

	private void onNotiTimeChange() {
		// send intent to service to notify the noti time change.
		Intent intent = new Intent(mContext, ShakeService.class);
		intent.setAction(ShakeService.ACTION_NOTI_TIME_CHANGE);
		mContext.startService(intent);
	}
}
