package com.mengjingtech.easyrecorder;

import java.lang.ref.WeakReference;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class BaseActivity extends Activity {
	private static final String TAG = "BaseActivity";
	private static final boolean DEBUG = ShakeConst.DEBUG;
	private static long AD_DELAY = 1000; // delay 1000ms to launch the ad.

	protected Controller controller;
	protected ProgressDialog pDialog;
	private LocalBroadcastManager mLocalBroadcastManager;
	private IntentFilter ifilter;
	protected Handler mUiHandler = null;

	private AdView adView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		controller = new Controller(getApplicationContext());
		mUiHandler = new UiHandler(this);
		controller.setUIHandler(mUiHandler);


		ifilter = new IntentFilter();
		registerBroadcast(ifilter);
		mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);
	}

	// by subclass to register the interested event
	protected void registerBroadcast(IntentFilter ifilter) {
	}

	// by subclass to handle the broadcat event
	protected boolean onReceiveBroadcast(Context context, Intent intent) {
		return false;
	}

	BroadcastReceiver mReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null) {
				String action = intent.getAction();
				if (DEBUG) {
					Log.d(TAG, "receive intent "
							+ (action != null ? action : "null"));
				}
				boolean handled = onReceiveBroadcast(context, intent);
				if (!handled) {
				}
			}
		}
	};

	@Override
	protected void onResume() {
		if (DEBUG)
			Log.d(TAG, "onResume");
		super.onResume();
		controller.setUIHandler(mUiHandler);
		if (adView != null) {
			adView.resume();
		}
	}

	@Override
	protected void onPause() {
		if (DEBUG)
			Log.d(TAG, "onPause");
		if (adView != null) {
			adView.pause();
		}
		super.onPause();

	}

	protected void showProcessingDialog() {
		showProcessingDialog(true);
	}

	protected void showProcessingDialog(boolean cancelable) {
		String processing = getResources().getString(R.string.processing);
		// show progress dialog
		pDialog = new ProgressDialog(this);
		pDialog.setMessage(processing);
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(cancelable);
		pDialog.show();
	}

	protected void dismissProcessingDialog() {
		if (pDialog != null) {
			pDialog.dismiss();
		}
	}

	@Override
	protected void onDestroy() {
		if (DEBUG)
			Log.d(TAG, "onDestroy");
		controller = null;
		super.onDestroy();
	}

	// should be override by subclass
	protected void handleMsg(android.os.Message msg) {
	}

	protected void toast(int resId) {
		Toast.makeText(this, resId, Toast.LENGTH_LONG).show();
	}

	// implement the UI handler

	static class UiHandler extends Handler {
		private WeakReference<BaseActivity> weakBaseActivity;

		public UiHandler(BaseActivity context) {
			weakBaseActivity = new WeakReference<BaseActivity>(context);
		};

		@Override
		public void handleMessage(android.os.Message msg) {
			BaseActivity baseActivity = weakBaseActivity.get();
			baseActivity.handleMsg(msg);
		}
	};

	@Override
	protected void onStart() {
		super.onStart();
		// ExitApplication.getInstance().addActivity(this);
		mLocalBroadcastManager.registerReceiver(mReceiver, ifilter);
	}

	@Override
	protected void onStop() {
		super.onStop();
		// ExitApplication.getInstance().removeActivity(this);
		mLocalBroadcastManager.unregisterReceiver(mReceiver);
	}

	protected void unLoadAdView() {
		((LinearLayout) adView.getParent()).removeAllViews();
		adView.destroy();
	}

	protected void loadAdView() {
		LinearLayout adviewLayout = (LinearLayout) findViewById(R.id.adviewLayout);
		if (adviewLayout == null) {
			return;
		}
		// Create an ad.
		if (adView == null) {
			adView = new AdView(this);
			adView.setAdSize(AdSize.BANNER);
			adView.setAdUnitId(getResources().getString(R.string.ad_unit_id));
			// Create an ad request.
			AdRequest adRequest = new AdRequest.Builder().build();
			// Start loading the ad in the background.
			adView.loadAd(adRequest);
			// Add the AdView to the view hierarchy. The view will have no size
			// until the ad is loaded.
			adviewLayout.addView(adView);
		} else {
			((LinearLayout) adView.getParent()).removeAllViews();
			adviewLayout.addView(adView);
			// Reload Ad if necessary. Loaded ads are lost when the activity is
			// paused.
			AdRequest adRequest = new AdRequest.Builder().build();
			// Start loading the ad in the background.
			adView.loadAd(adRequest);
		}
	}
}
