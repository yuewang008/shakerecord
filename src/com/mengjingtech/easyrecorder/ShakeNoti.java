package com.mengjingtech.easyrecorder;

import java.util.Date;

import com.mengjingtech.lib.Util;

public class ShakeNoti {
	private int id;
    private Date mDate;
    private boolean mEnabled = true;
    private int mPendingRecord;
    private boolean selected = false;
    
    public ShakeNoti(){
    	id = -1;
    };
    
    public String toString() {
        return "id = " + id + " date = " + mDate + " enabled = " + mEnabled;
    }
    
	public ShakeNoti(int id, Date date, boolean enabled) {
		super();
		this.id = id;
		this.mDate = date;
		this.mEnabled = enabled;
	}
	
	public ShakeNoti(int id, String date, boolean enabled) {
		super();
		this.id = id;
		this.mDate = Util.formatStringToTime(date);
		this.mEnabled = enabled;
	}
    
    // notify user there is some pending records.
    public void notifyUser() {
    	
    }

	public Date getDate() {
		return mDate;
	}

	public void setDate(Date date) {
		this.mDate = date;
	}

	public int getPendingRecord() {
		return mPendingRecord;
	}

	public void setPendingRecord(int pendingRecord) {
		this.mPendingRecord = pendingRecord;
	}

	public boolean isEnabled() {
		return mEnabled;
	}

	public void setEnabled(boolean enabled) {
		this.mEnabled = enabled;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}