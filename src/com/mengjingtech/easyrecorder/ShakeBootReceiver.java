package com.mengjingtech.easyrecorder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ShakeBootReceiver extends BroadcastReceiver {
    private static final boolean DEBUG = ShakeConst.DEBUG;
    private static final String TAG = "ShakeBootReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = null;

        if (intent != null) {
            action = intent.getAction();
        }
        if (DEBUG) Log.d(TAG, "onReceive: " + action);
        
        if (action != null) {
            if (Intent.ACTION_BOOT_COMPLETED.equals(action)) {
                if (DEBUG) Log.d(TAG, "start service");
                // start the background ShakeService after boot complete. 
                try {
                    Intent serviceIntent = new Intent(context,
                            ShakeService.class);
                    context.startService(serviceIntent);
                } catch (Exception e) {
                    Log.e("ShakeBootReceiver", e.getMessage());
                }
            } else if (action.equals(Intent.ACTION_NEW_OUTGOING_CALL)) {
                String phoneNumber = getResultData();
                if (phoneNumber == null) {
                    // No reformatted number, use the original
                    phoneNumber = intent
                            .getStringExtra(Intent.EXTRA_PHONE_NUMBER);
                }
                if (phoneNumber != null) {
                    try {
                        Intent serviceIntent = new Intent(context,
                                ShakeService.class);
                        serviceIntent
                                .setAction(ShakeService.ACTION_OUTGOING_CALL);
                        serviceIntent.putExtra(ShakeService.EXTRA_PHONENUMBER, phoneNumber);
                        context.startService(serviceIntent);
                    } catch (Exception e) {
                        Log.e("ShakeBootReceiver", e.getMessage());
                    }
                }
            }
        }
    }
}
