package com.mengjingtech.easyrecorder;

import java.util.ArrayList;

import com.mengjingtech.easyrecorder.R;

import android.os.Bundle;
import android.view.Menu;

public class ShakeRecordListStarActivity extends ShakeRecordListActivity {
	private static final boolean DEBUG = ShakeConst.DEBUG;
	private static final String TAG = "ShakeRecordListStarActivity";

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mList.clear();
		showStarred();
	}

	protected void onDateChanged() {
		showStarred();
	}

	private void showStarred() {
	    controller.loadMoreRecord(limit, offset, true);
	    showProcessingDialog();
	}

//	protected void onSyncFileDBDone() {
//		super.onSyncFileDBDone();
//		showStarred();
//	}
	
    protected void lastItemShow() {
        offset += limit;
        controller.loadMoreRecord(limit, offset, true);
    }

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		menu.removeItem(R.id.show_star);
		menu.removeItem(R.id.search);
		return true;
	}
	
    public void handleMsg(android.os.Message msg) {
        switch (msg.what) {
        case Controller.LOAD_MORE_RECORD_STARRED:
            if (msg.arg1 == 0) {
                mList.addAll(((ArrayList<ShakeFile>) msg.obj));
                mAdaptar.setList(mList);
                mAdaptar.notifyDataSetChanged();
            }

            dismissProcessingDialog();
            break;
        }
    }
    
    @Override
    protected void OnRecordChange() {
        mList.clear();

        if (sp.getBoolean(ShakeConst.PER_SYNC_FILE_DB_DONE, false)) {
            controller.loadMoreRecord(offset+limit, 0, true);
        }
    }
}
