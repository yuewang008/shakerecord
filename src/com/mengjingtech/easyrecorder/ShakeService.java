package com.mengjingtech.easyrecorder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.media.MediaRecorder;
import android.media.MediaRecorder.OnErrorListener;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Message;
import android.os.Messenger;
import android.os.PowerManager;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mengjingtech.lib.ShakeListener;
import com.mengjingtech.lib.ShakeListener.OnShakeListener;
import com.mengjingtech.lib.Util;

public class ShakeService extends Service {
	private static final boolean DEBUG = ShakeConst.DEBUG;
	private static final String TAG = "ShakeService";
	public static final String ACTION_NOTI_TIME_CHANGE = "com.mengjingtech.shake.ACTION_NOTI_TIME_CHANGE";
	public static final String ACTION_NOTI_TIMER_FIRED = "com.mengjingtech.shake.ACTION_NOTI_TIMER_FIRED";
	public static final String ACTION_OUTGOING_CALL = "com.mengjingtech.shake.ACTION_OUTGOING_CALL";
	public static final String EXTRA_PHONENUMBER = "PHONE_NUMBER";

	public static final String BIND_FROM = "bind_from";
	public static final byte BIND_FROM_RECORD_ACTIVITY = 1;

	ShakeListener mShakeListener = null;
	SharedPreferences mSharedPreferences = null;
	private static final long SHAKE_DEBOUNCE = 250; // debounce timer for 2
													// shake
													// gesture detection.

	private static final long SHAKE_VALID_TIMER = 1500; // the first shake valid
														// timer is 1500 ms.
	
	private static final long CHECK_PHONE_STATE_INTERVAL = 5000;

	private static final long VIBERATE_INTERVAL = 300;
	private static final long VIBERATE_DURATION = 200;

	private final static long[] VIB_PATTERN = { VIBERATE_INTERVAL,
			VIBERATE_DURATION, VIBERATE_INTERVAL, VIBERATE_DURATION };

	private static final int STATE_LISTENING = 0;
	private static final int STATE_FIRST_SHAKE_DEBOUNCE = 1;
	private static final int STATE_FIRST_SHAKE_VALID = 2;
	public float mDensity;

	private int mShakeDetectState = STATE_LISTENING;
	private String mCallNumber = "";
	private Messenger mActivityMessenger = null;
	private boolean mInCall = false;
	private ShakeFile mRecordFile = null;
	private int mRecordingTime = 0;
	private ShakeRecordActivity.State mRecordActivityState = ShakeRecordActivity.State.DESTROYED;

	private Timer timer = new Timer();

	// Binder given to clients
	private final IBinder mBinder = new ShakeServiceBinder();

	private MediaRecorder mMediaRecorder;

	private boolean mBinded = false;
	private Toast loadingToast = null;

	public enum RecordState {
		STOPPED, RECORDING, PAUSED
	}

	private List<File> mTmpFile = new ArrayList<File>();
	private int mSeagments = 1;
	private RecordState mRecordState = RecordState.STOPPED;

	/**
	 * Class used for the client Binder. Because we know this service always
	 * runs in the same process as its clients, we don't need to deal with IPC.
	 */
	public class ShakeServiceBinder extends Binder {
		public ShakeService getService() {
			// Return this instance of LocalService so clients can call public
			// methods
			return ShakeService.this;
		}

		public Messenger getMessenger() {
			// Return this instance of messenger
			// methods
			return mMessenger;
		}
	}

	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
			case ShakeConst.ServiceMessage.ACTIVITY_MESSENGER:
				if (DEBUG)
					Log.d(TAG, "get activity messenger");
				if (msg.replyTo != null) {
					mActivityMessenger = msg.replyTo;
				}
				break;
			case ShakeConst.ServiceMessage.SENSOR_SHAKE:
				PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
				if (pm != null && pm.isScreenOn()) {
					switch (mShakeDetectState) {
					case STATE_LISTENING:
						if (DEBUG)
							Log.d(TAG, "STATE_LISTENING");
						mShakeDetectState = STATE_FIRST_SHAKE_DEBOUNCE;
						mHandler.postDelayed(mShakeDebouncdRunnable,
								SHAKE_DEBOUNCE);
						break;
					case STATE_FIRST_SHAKE_DEBOUNCE:
						if (DEBUG)
							Log.d(TAG, "STATE_FIRST_SHAKE_DEBOUNCE");
						break;
					case STATE_FIRST_SHAKE_VALID:
						if (!mInCall
								&& mRecordActivityState != ShakeRecordActivity.State.FOREGROUND) {
							// do not launch record activity when call is
							// active.
							if (DEBUG)
								Log.d(TAG, "STATE_FIRST_SHAKE_VALID");
							showLoadingToast();
							Intent intent = new Intent(getApplicationContext(),
									navigator.class);
							intent.addFlags(intent.FLAG_ACTIVITY_NEW_TASK);
							intent.putExtra(ShakeConst.IS_LAUNCH_BY_SHAKE, true);
							startActivity(intent);
						}
						break;
					}
					break;
				}
				break;
			case ShakeConst.ServiceMessage.UPDATE_RECORD_TIME:
				mRecordingTime++;
				if (mRecordActivityState == ShakeRecordActivity.State.FOREGROUND) {
					// cancel any in recording notification when RecordActivity
					// is launched.
					NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
					notificationManager.cancel(ShakeConst.APP_NAME,
							R.id.notify_in_recording_id);

					try {
						if (mActivityMessenger != null) {
							if (DEBUG)
								Log.d(TAG, "send update record time");
							Message message = Message
									.obtain(null,
											ShakeConst.ServiceMessage.UPDATE_ACTIVITY_RECORD_TIME);
							message.arg1 = mRecordingTime;
							mActivityMessenger.send(message);
						}
					} catch (RemoteException e) {
						e.printStackTrace();
					}
				} else {
					// update the record in progress notification every 10
					// seconds
					if ((mRecordingTime % 10) == 0) {
						notifyInRecording(mRecordingTime);
					}
				}
				break;
			case ShakeConst.ServiceMessage.INCOMING_CALL_ACTIVE:
			case ShakeConst.ServiceMessage.OUTGOING_CALL_ACTIVE:
				if (mRecordActivityState != ShakeRecordActivity.State.DESTROYED
						&& mActivityMessenger != null) {
					try {
						if (DEBUG)
							Log.d(TAG, "ask recording activity to finish.");
						Message message = Message
								.obtain(null,
										ShakeConst.ServiceMessage.FINISH_RECORD_ACTIVITY);
						mActivityMessenger.send(message);
					} catch (RemoteException e) {
						e.printStackTrace();
					}
				}
				break;
			}
		}
	};

	private void showLoadingToast() {
		if (loadingToast == null) {
			LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
			View layout = inflater.inflate(R.layout.toast_1_line, null);

			TextView text = (TextView) layout.findViewById(R.id.toast_text);
			text.setText(R.string.loading);

			loadingToast = new Toast(getApplicationContext());
			loadingToast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
			loadingToast.setDuration(Toast.LENGTH_LONG);
			loadingToast.setView(layout);
		}
		loadingToast.show();
	}

	private Messenger mMessenger = new Messenger(mHandler);

	@Override
	public IBinder onBind(Intent intent) {
		byte bind_from = (byte) intent.getByteExtra(BIND_FROM, (byte) 0);
		if (bind_from == BIND_FROM_RECORD_ACTIVITY) {
			if (loadingToast != null) {
				loadingToast.cancel();
			}
		}
		return mBinder;
	}

	public String getOngoingCallNumber() {
		return mCallNumber;
	}

	public boolean isInCall() {
		return mInCall;
	}

	public int getRecordVolume() {
		return mMediaRecorder.getMaxAmplitude();
	}

	public ShakeFile getRecordFile() {
		return mRecordFile;
	}

	public boolean isRecording() {
		return (mRecordState == RecordState.RECORDING);
	}

	public void startRecording() {
		mRecordState = RecordState.RECORDING;
		File file = new File(Util.appSaveDir + (new Date().getTime())
				+ mSeagments + ShakeConst.SUFFIX);
		mTmpFile.add(file);
		mSeagments++;
		if (file.exists()) {
			if (file.delete())
				try {
					file.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
		} else {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		mMediaRecorder = new MediaRecorder();
		mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.AAC_ADTS);
		mMediaRecorder.setOutputFile(file.getAbsolutePath());
		mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
		mMediaRecorder.setOnErrorListener(new OnErrorListener() {
			@Override
			public void onError(MediaRecorder mr, int what, int extra) {
				mMediaRecorder.reset();
			}
		});
		try {
			mMediaRecorder.prepare();
			mMediaRecorder.start();
		} catch (Exception e) {
			e.printStackTrace();
			mMediaRecorder.release();
		} finally {

		}
		RecordTimerTask recordTimerTask = new RecordTimerTask();
		timer = new Timer("recordTimer", true);
		timer.schedule((TimerTask) recordTimerTask, 1000, 1000);
	}

	public void pauseRecording() {
		timer.cancel();
		mRecordState = RecordState.PAUSED;
		if (mMediaRecorder != null) {
			mMediaRecorder.stop();
			mMediaRecorder.release();
			mMediaRecorder = null;
		}
	}

	public void stopRecording() {
		timer.cancel();
		mRecordState = RecordState.STOPPED;
		if (mMediaRecorder != null) {
			mMediaRecorder.stop();
			mMediaRecorder.release();
			mMediaRecorder = null;
		}

		if (mTmpFile.size() == 0) {
			return;
		}

		String fileName;
		String note = "";
		if (mInCall) {
			// if incall, we add the call info into the filename.
			String contact = Util.getContactDisplayNameByNumber(this,
					mCallNumber);
			if (contact.trim().equals("")) {
				contact = mCallNumber;
			}
			fileName = contact + "_"
					+ Util.getTimeDateNameByTime(System.currentTimeMillis())
					+ ShakeConst.SUFFIX;
			note = Util.getLocaleDateTime(System.currentTimeMillis()) + contact;
		} else {
			fileName = Util.getDateTimeNameByTime(System.currentTimeMillis())
					+ ShakeConst.SUFFIX;
		}
		mRecordFile = new ShakeFile(fileName, ShakeFile.TYPE_3GP,
				mRecordingTime, note);

		if (mInCall) {
			if (DEBUG)
				Log.d(TAG, "finished recording is a call recording");
			mRecordFile.setCall(true);
		}

		File finalFile = new File(mRecordFile.getFile().getAbsolutePath());
		if (!finalFile.exists()) {
			try {
				finalFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		}

		FileOutputStream fileOutputStream = null;
		try {
			fileOutputStream = new FileOutputStream(finalFile);
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (int i = 0; i < mTmpFile.size(); i++) {
			File tmpFile = mTmpFile.get(i);
			FileInputStream fis = null;
			try {
				fis = new FileInputStream(tmpFile);
				byte[] tmpBytes = new byte[fis.available()];
				int length = tmpBytes.length;
				// The commented codes are for amr format handle
				// if (i == 0) {
				// while (fis.read(tmpBytes) != -1) {
				// fileOutputStream.write(tmpBytes, 0, length);
				// }
				// } else {
				// while (fis.read(tmpBytes) != -1) {
				// fileOutputStream.write(tmpBytes, 6, length - 6);
				// }
				// }
				while (fis.read(tmpBytes) != -1) {
					fileOutputStream.write(tmpBytes, 0, length);
				}

				fileOutputStream.flush();
				fis.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return;
			} catch (IOException e) {
				e.printStackTrace();
				return;
			} finally {
				fis = null;
			}
		}

		try {
			if (fileOutputStream != null)
				fileOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
			return;
		} finally {
			fileOutputStream = null;
		}
		for (File f : mTmpFile)
			f.delete();
		mTmpFile.clear();
		mSeagments = 1;

		mRecordFile.setTime(mRecordingTime);
		mRecordFile.setCreateTime(finalFile.lastModified());
		mRecordingTime = 0;
		// if the recording is not call, let the record activity to add the
		// record file.
		if (mRecordFile.isCall()) {
			ShakeFileManager.getInstance(getApplicationContext()).addFile(
					mRecordFile);

			// cancel any in recording notification when call recording
			// finished.
			NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			notificationManager.cancel(ShakeConst.APP_NAME,
					R.id.notify_in_recording_id);
		}
	}

	private void notifyInRecording(int recordingTime) {
		Resources res = getResources();
		String notiString = String.format(res.getString(R.string.in_recording),
				Util.getStandardTime(recordingTime));

		Intent notificationIntent = new Intent(this, ShakeRecordActivity.class);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent contentItent = PendingIntent.getActivity(this, 0,
				notificationIntent, 0);

		CharSequence contentTitle = res.getString(R.string.app_name);
		CharSequence contentText = notiString;
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				this).setSmallIcon(R.drawable.logo)
				.setContentTitle(contentTitle).setContentText(contentText);
		mBuilder.setContentIntent(contentItent);
		mBuilder.setAutoCancel(true);
		mBuilder.setOngoing(true);
		mBuilder.setDefaults(Notification.DEFAULT_LIGHTS);

		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(ShakeConst.APP_NAME,
				R.id.notify_in_recording_id, mBuilder.build());
	}

	private void notifyCallRecordingComplete() {
		Resources res = getResources();
		String notiString = String.format(
				res.getString(R.string.call_recording_complete),
				mRecordFile.getFileName());

		Intent notificationIntent = new Intent(this,
				ShakeRecordListFullActivity.class);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		notificationIntent.putExtra(ShakeConst.REFRESH, true);
		PendingIntent contentItent = PendingIntent.getActivity(this, 0,
				notificationIntent, 0);

		CharSequence contentTitle = res.getString(R.string.app_name);
		CharSequence contentText = notiString;
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				this).setSmallIcon(R.drawable.logo)
				.setContentTitle(contentTitle).setContentText(contentText);
		mBuilder.setContentIntent(contentItent);
		mBuilder.setAutoCancel(true);
		mBuilder.setDefaults(Notification.DEFAULT_ALL);

		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(ShakeConst.APP_NAME,
				R.id.notify_call_recording_id, mBuilder.build());
	}

	public RecordState getRecordState() {
		return mRecordState;
	}

	class RecordTimerTask extends TimerTask {
		@Override
		public void run() {
			Message message = new Message();
			message.what = ShakeConst.ServiceMessage.UPDATE_RECORD_TIME;
			mHandler.sendMessage(message);
		}
	};

	@Override
	public void onCreate() {
		super.onCreate();

		mShakeListener = new ShakeListener(this);
		mShakeListener.setOnShakeListener(mOnShakeListener);

		mSharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(this);

		TelephonyManager telephonyManager = (TelephonyManager) getApplicationContext()
				.getSystemService(Context.TELEPHONY_SERVICE);
		telephonyManager.listen(mPhoneStateListener,
				PhoneStateListener.LISTEN_CALL_STATE);

		DisplayMetrics dm = getResources().getDisplayMetrics();
		mDensity = dm.density;

		setNotifyAlarm();

		bindToService();
	}

	private void setNotifyAlarm() {
		// Calendar comingAlarm = ShakeNotiManager.getInstance(
		// getApplicationContext()).getComingAlarm(new Date());
	}

	@Override
	public void onDestroy() {
		if (mFloatView != null) {
			mWindowManager.removeView(mFloatView);
		}
		timer.cancel();
		mHandler.removeCallbacksAndMessages(null);
		super.onDestroy();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (intent == null) {
			return super.onStartCommand(intent, flags, startId);
		}

		String action = intent.getAction();
		if (DEBUG) {
			Log.d(TAG, "onStartCommand " + action);
		}

		// when notification timer fired or changed, set a new notification
		// timer
		if (ACTION_NOTI_TIME_CHANGE.equals(action)
				|| ACTION_NOTI_TIMER_FIRED.equals(action)) {
			ShakeNotiManager snm = ShakeNotiManager
					.getInstance(getApplicationContext());
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			Date now = cal.getTime();
			Calendar calnow = snm.getComingAlarm(now);

			AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
			PendingIntent pi = PendingIntent.getBroadcast(this, 0, new Intent(
					this, ShakeAlarmReceiver.class), 0);
			am.set(AlarmManager.RTC_WAKEUP, calnow.getTimeInMillis(), pi);
		} else if (ACTION_OUTGOING_CALL.equals(action)) {
			createView();
			mInCall = true;
			mCallNumber = intent.getStringExtra(EXTRA_PHONENUMBER);
			Message message = Message.obtain(null,
					ShakeConst.ServiceMessage.OUTGOING_CALL_ACTIVE);
			message.obj = mCallNumber;
			mHandler.sendMessage(message);
		}

		final IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_TIME_TICK);
		filter.addAction(Intent.ACTION_NEW_OUTGOING_CALL);
		filter.setPriority(Integer.MAX_VALUE);
		registerReceiver(mReceiver, filter);

		return START_STICKY;
	}

	private BroadcastReceiver mReceiver = new BroadcastReceiver() {
		private static final String TAG = "ScreenOnReceiver";

		@Override
		public void onReceive(Context context, Intent intent) {

			final String action = intent.getAction();
			if (DEBUG)
				Log.d(TAG, "service receive:" + action);

			boolean isServiceRunning = false;

			if (action.equals(Intent.ACTION_TIME_TICK)) {
				ActivityManager manager = (ActivityManager) getApplicationContext()
						.getSystemService(Context.ACTIVITY_SERVICE);
				for (RunningServiceInfo service : manager
						.getRunningServices(Integer.MAX_VALUE)) {
					if ("com.mengjingtech.easyrecorder.ShakeService"
							.equals(service.service.getClassName())) {
						isServiceRunning = true;
					}
				}
				if (!isServiceRunning) {
					Intent i = new Intent(context, ShakeService.class);
					context.startService(i);
				}
			}
		}
	};

	private PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
		@Override
		public void onCallStateChanged(int state, String incomingNumber) {
			super.onCallStateChanged(state, incomingNumber);

			switch (state) {
			case TelephonyManager.CALL_STATE_RINGING:
				createView();
				mInCall = true;
				mCallNumber = incomingNumber;
				if (DEBUG)
					Log.d(TAG, "incoming call ringing!");
				break;
			case TelephonyManager.CALL_STATE_IDLE:
				onCallEnded();
				break;

			case TelephonyManager.CALL_STATE_OFFHOOK:
				mInCall = true;
				if (DEBUG)
					Log.d(TAG, "state = " + state + "incomingNumber = "
							+ incomingNumber);
				Message message = Message.obtain(null,
						ShakeConst.ServiceMessage.INCOMING_CALL_ACTIVE);
				message.obj = incomingNumber;
				mHandler.sendMessage(message);
				break;
			}
		}
	};

	private void onCallEnded() {
		mHandler.removeCallbacks(mCheckPhoneStateRunnable);

		if (mFloatView != null && mFloatView.getParent() != null) {
			mWindowManager.removeView(mFloatView);
		}

		if (mRecordState != RecordState.STOPPED) {
			stopRecording();
			notifyCallRecordingComplete();
			mFloatImage.setImageResource(R.drawable.record_green_button);
		}

		if (DEBUG)
			Log.d(TAG, "phone call ended!");

		if (mActivityMessenger != null) {
			try {
				Message message = Message.obtain(null,
						ShakeConst.ServiceMessage.CALL_END);
				mActivityMessenger.send(message);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}

		mInCall = false;
	}

	private void syncRecordDB() {

	}

	private OnShakeListener mOnShakeListener = new OnShakeListener() {
		@Override
		public void onShake() {
			boolean enabled = mSharedPreferences.getBoolean(
					ShakeConst.PER_KEY_SHAKE_ENABLE, true);
			if (DEBUG)
				Log.d(TAG, "onshakeListener enable = " + enabled);
			if (enabled) {
				Message msg = new Message();
				msg.what = ShakeConst.ServiceMessage.SENSOR_SHAKE;
				mHandler.sendMessage(msg);
			}
		}
	};

	private Runnable mShakeDebouncdRunnable = new Runnable() {
		@Override
		public void run() {
			if (DEBUG)
				Log.d(TAG, "mShakeDebouncdRunnable");
			mShakeDetectState = STATE_FIRST_SHAKE_VALID;
			mHandler.removeCallbacks(mShakeValidRunnable);
			mHandler.postDelayed(mShakeValidRunnable, SHAKE_VALID_TIMER);
		}
	};

	private Runnable mShakeValidRunnable = new Runnable() {
		@Override
		public void run() {
			if (DEBUG)
				Log.d(TAG, "mShakeValidRunnable");
			mShakeDetectState = STATE_LISTENING;
		}
	};

	private Runnable mCheckPhoneStateRunnable = new Runnable() {
		@Override
		public void run() {
			if (DEBUG)
				Log.d(TAG, "mShakeDebouncdRunnable");
			TelephonyManager telephonyManager = (TelephonyManager) getApplicationContext()
					.getSystemService(Context.TELEPHONY_SERVICE);
			if (telephonyManager.getCallState() == TelephonyManager.CALL_STATE_IDLE) {
				onCallEnded();
			} else {
				mHandler.postDelayed(mCheckPhoneStateRunnable, CHECK_PHONE_STATE_INTERVAL);
			}
		}
	};

	private OnClickListener mOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (mMoved) {
				// if it is a move, do not trigger the click event.
				return;
			}

			if (getRecordState() == RecordState.STOPPED
					|| getRecordState() == RecordState.PAUSED) {
				startRecording();
				mFloatImage
						.setImageResource(R.drawable.pause_circle_red_button);
			} else {
				if (DEBUG)
					Log.d(TAG, "pause recording");
				pauseRecording();
				mFloatImage.setImageResource(R.drawable.record_green_button);
			}
		}
	};

	private float mDownX, mDownY;
	private boolean mMoved = false;

	private OnTouchListener mOnTouchListener = new OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {

			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				mMoved = false;
				mDownX = event.getRawX();
				mDownY = event.getRawY();
				break;
			case MotionEvent.ACTION_MOVE:
				if (event.getRawX() - mDownX > 5
						|| event.getRawY() - mDownY > 5) {
					mMoved = true;
				}
				updateViewPosition((int) event.getRawX(), (int) event.getRawY());
				break;
			case MotionEvent.ACTION_UP:
				if (mMoved) {
					int x = (int) event.getRawX();
					int y = (int) event.getRawY();
					updateViewPosition(x, y);

					SharedPreferences.Editor editor = mSharedPreferences.edit();
					editor.putInt(ShakeConst.FLOAT_VIEW_X, x);
					editor.putInt(ShakeConst.FLOAT_VIEW_Y, y);
					editor.commit();
				}
				break;
			}
			return false;
		}
	};

	private void updateViewPosition(int x, int y) {
		wmParams.x = x - mFloatView.getWidth() / 2;
		wmParams.y = (int) (y - mFloatView.getHeight() / 2 - (25 * mDensity));
		wmParams.gravity = Gravity.LEFT | Gravity.TOP;
		mWindowManager.updateViewLayout(mFloatView, wmParams);
	}

	WindowManager mWindowManager;
	WindowManager.LayoutParams wmParams;
	RelativeLayout mFloatView;
	ImageView mFloatImage;

	@SuppressWarnings("deprecation")
	private void createView() {
		boolean leftHand = mSharedPreferences.getBoolean(
				ShakeConst.PER_KEY_LEFT_HAND, false);
		mWindowManager = (WindowManager) getApplicationContext()
				.getSystemService(Context.WINDOW_SERVICE);

		wmParams = ((ShakeRecordApp) getApplication()).getWindowParams();

		wmParams.type = LayoutParams.TYPE_SYSTEM_ERROR;
		wmParams.format = PixelFormat.RGBA_8888;
		wmParams.flags = LayoutParams.FLAG_NOT_FOCUSABLE
				| LayoutParams.FLAG_SHOW_WHEN_LOCKED
				| LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON;
		wmParams.gravity = Gravity.LEFT | Gravity.TOP;

		wmParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
		wmParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

		LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);// LayoutInflater.from(getApplication());

		if (mFloatView == null) {
			mFloatView = (RelativeLayout) inflater.inflate(R.layout.float_view,
					null);
			mFloatView.setOnTouchListener(mOnTouchListener);
			mFloatView.setOnClickListener(mOnClickListener);
			mFloatImage = (ImageView) mFloatView
					.findViewById(R.id.recordButton);
		} else {
			if (mFloatView.getParent() != null) {
				mWindowManager.removeViewImmediate(mFloatView);
			}
		}

		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		int x = sharedPreferences.getInt(ShakeConst.FLOAT_VIEW_X, -1000);
		int y = sharedPreferences.getInt(ShakeConst.FLOAT_VIEW_Y, -1000);

		Point point = new Point();
		mWindowManager.getDefaultDisplay().getSize(point);

		if (x < -200) {
			wmParams.x = (int) (point.x * 0.75);
			wmParams.y = point.y / 2;
		} else {
			wmParams.x = x;
			wmParams.y = y;
		}

		mFloatImage.setImageResource(R.drawable.record_green_button);
		mWindowManager.addView(mFloatView, wmParams);
		updateViewPosition(wmParams.x, wmParams.y);
		mHandler.postDelayed(mCheckPhoneStateRunnable, CHECK_PHONE_STATE_INTERVAL);
	}

	public void setRecordActivityState(ShakeRecordActivity.State state) {
		mRecordActivityState = state;
		if (mRecordActivityState == ShakeRecordActivity.State.DESTROYED) {
			mActivityMessenger = null;
			// if RecordActivity was killed by system, notify user there is a
			// processing recording.
			if (isRecording()) {
				notifyInRecording(mRecordingTime);
			}
		}
	}

	public void cancelLoadingToast() {
		if (loadingToast != null) {
			loadingToast.cancel();
		}
	}

	private void bindToService() {
		Intent intent = new Intent(
				"com.mengjingtech.easyrecorder.MONITOR_SERVICE");
		intent.setPackage("com.mengjingtech.easyrecorder");
		bindService(intent, mServiceConnection, BIND_AUTO_CREATE);
	}

	private ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName comp, IBinder binder) {
			if (DEBUG)
				Log.d(TAG, "onServiceConnected");
			mBinded = true;
			try {
				binder.linkToDeath(mDeathRecipient, 0);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName comp) {
			if (DEBUG)
				Log.d(TAG, "onServiceDisconnected");
			mBinded = false;
			bindToService();
		}
	};

	DeathRecipient mDeathRecipient = new DeathRecipient() {
		@Override
		public void binderDied() {
			bindToService();
		}
	};
}
