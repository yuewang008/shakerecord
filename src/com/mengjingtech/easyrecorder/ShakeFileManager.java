package com.mengjingtech.easyrecorder;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mengjingtech.lib.Util;

public class ShakeFileManager {
	private static final boolean DEBUG = ShakeConst.DEBUG;
	private static final String TAG = "ShakeFileManager";

	private static ShakeFileManager mShakeFileManager = null;
	private DataChangeListener dataChangeListener = null;

	private ShakeDBHelper mShakeDBHelper;

	// store the 4 time heads: today, yesterday, 2 days ago, a week ago.
	protected ArrayList<ShakeFile> headList = new ArrayList<ShakeFile>();
	// store the count for 4 time types.
	protected int[] listCount = new int[ShakeFile.LIST_TYPE_TOTAL];
	private long[] dateArray = { 0, 0, 0, 0, 0 };
	private int[] pastDays = { 1, 0, -1, -6 };

	public static ShakeFileManager getInstance(Context context) {
		if (mShakeFileManager == null) {
			mShakeFileManager = new ShakeFileManager(context);
		}
		return mShakeFileManager;
	}

	private ShakeFileManager(Context context) {
		for (int i = 0; i < listCount.length; i++) {
			listCount[i] = 0;
		}
		mShakeDBHelper = new ShakeDBHelper(context, ShakeDBHelper.DBNAME, null,
				1);
	}

	private void addListHeads() {
		// get today's calendar instance.
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(System.currentTimeMillis());
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);

		for (int i = 0; i < pastDays.length; i++) {
			cal.add(Calendar.DAY_OF_YEAR, pastDays[i]);
			ShakeFile sf = new ShakeFile(true, ShakeFile.LIST_TODAY + i,
					cal.getTimeInMillis());
			// mList.add(sf);
			headList.add(sf);
			dateArray[i] = cal.getTimeInMillis();
			cal.add(Calendar.DAY_OF_YEAR, -pastDays[i]);
		}
	}

	public ArrayList<ShakeFile> getHeadList() {
		return headList;
	}

	public ArrayList<ShakeFile> getMoreRecord(int limit, int offset) {
		return getMoreRecord(limit, offset, null);
	}

	public ArrayList<ShakeFile> getMoreRecord(int limit, int offset,
			String query) {
		return getMoreRecord(limit, offset, query, false);
	}

	public ArrayList<ShakeFile> getMoreRecord(int limit, int offset,
			boolean starred) {
		return getMoreRecord(limit, offset, null, starred);
	}

	private ArrayList<ShakeFile> getMoreRecord(int limit, int offset,
			String query, boolean starred) {
		Cursor cr = null;
		ArrayList<ShakeFile> list = new ArrayList<ShakeFile>();
		String sql;
		if (starred) {
			sql = "SELECT * FROM " + ShakeDBHelper.TABLE_NAME
					+ " WHERE star = 1 ORDER BY ctime DESC LIMIT " + limit
					+ " offset " + offset;
		} else if (query != null && query.length() > 0) {
			sql = "SELECT * FROM " + ShakeDBHelper.TABLE_NAME
					+ " WHERE name LIKE '%" + query
					+ "%' ORDER BY ctime DESC LIMIT " + limit + " offset "
					+ offset;
		} else {
			sql = "SELECT * FROM " + ShakeDBHelper.TABLE_NAME
					+ " ORDER BY ctime DESC LIMIT " + limit + " offset "
					+ offset;
		}
		SQLiteDatabase db = mShakeDBHelper.getWritableDatabase();
		cr = db.rawQuery(sql, null);
		while (cr.moveToNext()) {
			int id = cr.getInt(cr.getColumnIndex(ShakeDBHelper.FIELD_ID));
			String fileName = cr.getString(cr
					.getColumnIndex(ShakeDBHelper.FIELD_NAME));
			boolean unread = cr.getInt(cr
					.getColumnIndex(ShakeDBHelper.FIELD_UNREAD)) == 0 ? false
					: true;
			boolean isStar = cr.getInt(cr
					.getColumnIndex(ShakeDBHelper.FIELD_STAR)) == 0 ? false
					: true;
			boolean isCall = cr.getInt(cr
					.getColumnIndex(ShakeDBHelper.FIELD_CALL)) == 0 ? false
					: true;
			int time = cr.getInt(cr.getColumnIndex(ShakeDBHelper.FIELD_TIME));
			long createTime = cr.getLong(cr
					.getColumnIndex(ShakeDBHelper.FIELD_CTIME));
			ShakeFile sf = new ShakeFile(id, fileName, unread, time,
					createTime, isStar, isCall);
			list.add(sf);
			// checkFileDate(sf);
		}
		cr.close();
		return list;
	}

	public class FileItem {
		public int id;
		public boolean exist = false;

		FileItem(int id, boolean exist) {
			this.id = id;
			this.exist = exist;
		}
	}

	public HashMap<String, FileItem> getFileIDDB() {
		Cursor cr = null;
		HashMap<String, FileItem> fileMap = new HashMap<String, FileItem>();
		String sql = "SELECT " + ShakeDBHelper.FIELD_ID + ", "
				+ ShakeDBHelper.FIELD_NAME + " FROM "
				+ ShakeDBHelper.TABLE_NAME;
		SQLiteDatabase db = mShakeDBHelper.getWritableDatabase();
		cr = db.rawQuery(sql, null);
		while (cr.moveToNext()) {
			int id = cr.getInt(cr.getColumnIndex(ShakeDBHelper.FIELD_ID));
			String fileName = cr.getString(cr
					.getColumnIndex(ShakeDBHelper.FIELD_NAME));
			fileMap.put(fileName, new FileItem(id, false));
		}
		cr.close();
		return fileMap;
	}

	public ArrayList<File> scanFiletoList(String path) {
		ArrayList<File> fileList = new ArrayList<File>();
		File dir = new File(path);
		File[] subFiles = dir.listFiles();
		if (subFiles != null)
			for (File f : subFiles) {
				ShakeFile shakeFile = checkFileInDB(f);
				if (shakeFile != null) {
					// mList.add(shakeFile);
				} else {
					shakeFile = new ShakeFile(f);
					addFileToDB(shakeFile);
					// mList.add(shakeFile);
				}
				// checkFileDate(f.lastModified());
				checkFileDate(shakeFile);
			}
		return fileList;
	}

	// try to find the ShakeFile from DB with the file.
	private ShakeFile checkFileInDB(File f) {
		Cursor cr = null;
		String sql = "SELECT * FROM " + ShakeDBHelper.TABLE_NAME + " WHERE "
				+ ShakeDBHelper.FIELD_NAME + "=?";
		SQLiteDatabase db = mShakeDBHelper.getWritableDatabase();
		cr = db.rawQuery(sql, new String[] { f.getName() });
		if (cr.getCount() == 0) {
			return null;
		}
		cr.moveToNext();
		int id = cr.getInt(cr.getColumnIndex(ShakeDBHelper.FIELD_ID));
		boolean unread = cr.getInt(cr
				.getColumnIndex(ShakeDBHelper.FIELD_UNREAD)) == 0 ? false
				: true;
		int time = cr.getInt(cr.getColumnIndex(ShakeDBHelper.FIELD_TIME));
		long createTime = cr.getLong(cr
				.getColumnIndex(ShakeDBHelper.FIELD_CTIME));
		boolean isStar = cr.getInt(cr.getColumnIndex(ShakeDBHelper.FIELD_STAR)) == 0 ? false
				: true;
		boolean isCall = cr.getInt(cr.getColumnIndex(ShakeDBHelper.FIELD_CALL)) == 0 ? false
				: true;
		cr.close();
		return new ShakeFile(id, f, unread, time, isStar, isCall);
	}

	private void checkFileDate(ShakeFile sf) {
		long dateMills = sf.getCreateTime();
		for (int i = 0; i < listCount.length; i++) {
			if (dateMills >= dateArray[i + 1]) {
				listCount[i]++;
				sf.setListType(i);
				break;
			}
		}
	}

	private void clear() {
		// mList.clear();
		for (int i = 0; i < listCount.length; i++) {
			listCount[i] = 0;
		}
	}

	public void scanFiles(String path) {
		clear();
		addListHeads();
		File dir = new File(path);
		File[] subFiles = dir.listFiles();
		if (subFiles != null)
			for (File f : subFiles) {
				ShakeFile shakeFile = checkFileInDB(f);
				if (shakeFile != null) {
					// mList.add(shakeFile);
				} else {
					shakeFile = new ShakeFile(f);
					addFileToDB(shakeFile);
					// mList.add(shakeFile);
				}
				checkFileDate(shakeFile);
			}
	}

	private void sort(List<ShakeFile> list) {
		Collections.sort(list, new Comparator<ShakeFile>() {
			@Override
			public int compare(ShakeFile lhs, ShakeFile rhs) {
				long timel, timer;
				if (lhs.isHead()) {
					timel = lhs.getHeadTime();
				} else {
					timel = lhs.getCreateTime();
				}

				if (rhs.isHead()) {
					timer = rhs.getHeadTime();
				} else {
					timer = rhs.getCreateTime();
				}

				if (timel < timer) {
					return 1;
				} else if (timel == timer) {
					return 0;
				} else {
					return -1;
				}
			}
		});
	}
	
	public boolean isFileExist(ShakeFile sf) {
		boolean fileExist = false;
		ContentValues cv = new ContentValues();
		SQLiteDatabase db = mShakeDBHelper.getWritableDatabase();
		Cursor cr = db.query(true, ShakeDBHelper.TABLE_NAME, new String[] {ShakeDBHelper.FIELD_ID}, ShakeDBHelper.FIELD_NAME
				+ " = ?", new String[] { sf.getFileName() }, null, null, null, null);
		if (cr.getCount() > 0) {
			fileExist = true;
		}
		return fileExist;
	}

	public void addFileToDB(ShakeFile sf) {
		ContentValues cv = new ContentValues();
		cv.put(ShakeDBHelper.FIELD_NAME, sf.getFileName());
		cv.put(ShakeDBHelper.FIELD_UNREAD, 1);
		cv.put(ShakeDBHelper.FIELD_TIME, sf.getTime());
		cv.put(ShakeDBHelper.FIELD_CTIME, sf.getFile().lastModified());
		cv.put(ShakeDBHelper.FIELD_STAR, String.valueOf(sf.isStar() ? 1 : 0));
		cv.put(ShakeDBHelper.FIELD_CALL, String.valueOf(sf.isCall() ? 1 : 0));
		SQLiteDatabase db = mShakeDBHelper.getWritableDatabase();
		db.insertOrThrow(ShakeDBHelper.TABLE_NAME, null, cv);
	}

	private void updateItemInDB(ShakeFile sf) {
		ContentValues cv = new ContentValues();
		cv.put(ShakeDBHelper.FIELD_NAME, sf.getFileName());
		cv.put(ShakeDBHelper.FIELD_UNREAD,
				String.valueOf(sf.isUnread() ? 1 : 0));
		cv.put(ShakeDBHelper.FIELD_TEXT, sf.getText());
		cv.put(ShakeDBHelper.FIELD_TIME, sf.getTime());
		cv.put(ShakeDBHelper.FIELD_CTIME, sf.getFile().lastModified());
		cv.put(ShakeDBHelper.FIELD_STAR, String.valueOf(sf.isStar() ? 1 : 0));
		cv.put(ShakeDBHelper.FIELD_CALL, String.valueOf(sf.isCall() ? 1 : 0));
		SQLiteDatabase db = mShakeDBHelper.getWritableDatabase();
		db.update(ShakeDBHelper.TABLE_NAME, cv,
				ShakeDBHelper.FIELD_ID + " = ?",
				new String[] { String.valueOf(sf.getId()) });
	}

	private void updateItemInDBWithName(ShakeFile sf) {
		ContentValues cv = new ContentValues();
		cv.put(ShakeDBHelper.FIELD_NAME, sf.getFileName());
		cv.put(ShakeDBHelper.FIELD_UNREAD,
				String.valueOf(sf.isUnread() ? 1 : 0));
		cv.put(ShakeDBHelper.FIELD_TEXT, sf.getText());
		cv.put(ShakeDBHelper.FIELD_TIME, sf.getTime());
		cv.put(ShakeDBHelper.FIELD_CTIME, sf.getFile().lastModified());
		cv.put(ShakeDBHelper.FIELD_STAR, String.valueOf(sf.isStar() ? 1 : 0));
		cv.put(ShakeDBHelper.FIELD_CALL, String.valueOf(sf.isCall() ? 1 : 0));
		SQLiteDatabase db = mShakeDBHelper.getWritableDatabase();
		db.update(ShakeDBHelper.TABLE_NAME, cv, ShakeDBHelper.FIELD_NAME
				+ " = ?", new String[] { sf.getFileName() });
	}

	public void updateFile(ShakeFile sf) {
		updateItemInDB(sf);
		onFileChange();
	}

	public void updateFileWithName(ShakeFile sf) {
		updateItemInDBWithName(sf);
		onFileChange();
	}

	public void addFile(ShakeFile sf) {
		addFileToDB(sf);
		checkFileDate(sf);
		onFileChange();
	}

	public void deleteFile(ShakeFile sf) {
		// delete from DB.
		deleteFromDBbyName(sf.getFileName());
		listCount[sf.getListType()]--;
		// delete file.
		sf.delete();
		onFileChange();
	}

	public void deleteFile(ArrayList<ShakeFile> list, int position) {
		ShakeFile sf = list.get(position);
		deleteFile(list, sf);
	}

	public void deleteFile(ArrayList<ShakeFile> list, ShakeFile sf) {
		// delete from list.
		list.remove(sf);
		// delete from DB.
		deleteFromDBbyName(sf.getFileName());
		listCount[sf.getListType()]--;
		// delete file.
		sf.delete();
		onFileChange();
	}

	public void deleteFiles(ArrayList<ShakeFile> list,
			ArrayList<ShakeFile> deleteItems) {
		if (deleteItems.size() <= 0) {
			return;
		}
		list.removeAll(deleteItems);
		for (ShakeFile sf : deleteItems) {
			if (deleteFromDBbyID(sf.getId()) < 1) {
				Log.e(TAG, "error delete record file!");
			} else {
				sf.delete();
				listCount[sf.getListType()]--;
			}
		}
		onFileChange();
	}

	public int deleteFromDBbyID(int id) {
		SQLiteDatabase db = mShakeDBHelper.getWritableDatabase();
		return db.delete(ShakeDBHelper.TABLE_NAME, "id = ?",
				new String[] { String.valueOf(id) });
	}

	private int deleteFromDBbyName(String name) {
		SQLiteDatabase db = mShakeDBHelper.getWritableDatabase();
		return db.delete(ShakeDBHelper.TABLE_NAME, "name = ?",
				new String[] { name });
	}

	public ShakeFile getFile(ArrayList<ShakeFile> list, int position) {
		return list.get(position);
	}
	
	public ShakeFile getFileByName(ArrayList<ShakeFile> list, String name) {
		for (ShakeFile sf : list) {
			if (sf.getFileName().equals(name)) {
				return sf;
			}
		}
		return null;
	}

	interface DataChangeListener {
		public void onDataChanged();
	}

	public void registerDataChangeListener(DataChangeListener listener) {
		mDataChangeListenerList.add(listener);
	}

	public void unregisterDataChangeListener(DataChangeListener listener) {
		mDataChangeListenerList.remove(listener);
	}

	private ArrayList<DataChangeListener> mDataChangeListenerList = new ArrayList<DataChangeListener>();

	private void onFileChange() {
		for (DataChangeListener dcl : mDataChangeListenerList) {
			dcl.onDataChanged();
		}
	}

	public boolean hasPendingRedording(ArrayList<ShakeFile> list) {
		for (ShakeFile sf : list) {
			if (sf.isUnread()) {
				return true;
			}
		}
		return false;
	}

	public void syncFileDBDone() {
		onFileChange();
	}

	// change the file playing state and return old state
	public int setFilePlayState(ArrayList<ShakeFile> list, int position,
			int state) {
		int oldState = list.get(position).getState();
		list.get(position).setState(state);
		onFileChange();
		return oldState;
	}

}
