package com.mengjingtech.easyrecorder;

import java.util.ArrayList;

import android.os.Bundle;
import android.util.Log;

public class ShakeRecordListFullActivity extends ShakeRecordListActivity {
	private static final boolean DEBUG = ShakeConst.DEBUG;
	private static final String TAG = "ShakeRecordListFullActivity";

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (DEBUG)
			Log.d(TAG, "onCreate");

		mList.clear();

		if (sp.getBoolean(ShakeConst.PER_SYNC_FILE_DB_DONE, false)) {
			if (DEBUG)
				Log.d(TAG, "loadMoreRecord + limit=" + limit + " offset="
						+ offset);
			controller.loadMoreRecord(limit, offset);
		}
	}

	protected void lastItemShow() {
		offset += limit;
		controller.loadMoreRecord(limit, offset);
	}

	public void handleMsg(android.os.Message msg) {
		switch (msg.what) {
		case Controller.LOAD_MORE_RECORD:
			if (DEBUG)
				Log.d(TAG, "get loadMoreRecord");
			if (msg.arg1 == 0) {
				mList.addAll(((ArrayList<ShakeFile>) msg.obj));
				mAdaptar.setList(mList);
				mAdaptar.notifyDataSetChanged();

				if (playerService != null && (playerService.isPlaying() || playerService.isPaused())) {
					ShakeFile sf = ShakeFileManager.getInstance(
							getApplicationContext()).getFileByName(mList,
							playerService.getPlayFile().getFileName());
					if (sf != null) {
						if (playerService.isPlaying()) {
							sf.setState(ShakeFile.STATE_PLAYING);
						} else {
							sf.setState(ShakeFile.STATE_PAUSED);
						}
						playerService.setPlayFile(sf);
					}

					mHandler.removeCallbacks(updateThread);
					mHandler.post(updateThread);
				}
			}
			break;
		}
	}

	@Override
	protected void OnRecordChange() {
		mList.clear();

		if (sp.getBoolean(ShakeConst.PER_SYNC_FILE_DB_DONE, false)) {
			controller.loadMoreRecord(offset + limit, 0);
		}
	}
}
