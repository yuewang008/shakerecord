package com.mengjingtech.easyrecorder;

import java.io.File;

import com.mengjingtech.lib.Util;

public class ShakeFile {
    private int id;
    private int mType;
    private File mFile;
    private int mTime;
    private long createTime;
    private boolean star;
    private boolean unread;
    private String text;
    private int mState = STATE_STOPED;
    private boolean isHead = false;
    private int headType = 0;
    private long headTime = 0L;
    private int listType = 0;
    private boolean collapsed = false;
    private boolean selected = false;
    private boolean call = false;

    public static final int TYPE_3GP = 1;
    public static final int TYPE_MP3 = 2;

    public static final int STATE_RESET = 1;
    public static final int STATE_PREPARED = 2;
    public static final int STATE_PLAYING = 3;
    public static final int STATE_PAUSED = 4;
    public static final int STATE_STOPED = 5;
    
    public static final int LIST_TODAY = 0;
    public static final int LIST_YESTERDAY = 1;
    public static final int LIST_TWO_DAYS_AGO = 2;
    public static final int LIST_WEEK_AGO = 3;
    public static final int LIST_TYPE_TOTAL = LIST_WEEK_AGO + 1; // Should be

    public ShakeFile(boolean isHead, int headType, long headTime) {
        super();
        this.isHead = isHead;
        this.headType = headType;
        this.headTime = headTime;
    }

    public ShakeFile(String name, int type, int time, String note) {
        this(-1, name, type, true, note, time, 0L, false, false);
    }

    public ShakeFile(int id, String name, boolean unread, int time,
            long createTime, boolean isStar, boolean isCall) {
        this(id, name, TYPE_3GP, unread, "", time, createTime, isStar, isCall);
    }

    public ShakeFile(int id, String name, boolean unread, String text, int time) {
        this(id, name, TYPE_3GP, unread, text, time, 0L, false, false);
    }

    public ShakeFile(int id, String name, int type, boolean unread,
            String text, int time, long createTime, boolean isStar,
            boolean isCall) {
        this.id = id;
        this.mFile = new File(Util.getStoragePath() + name);
        this.mType = type;
        this.unread = true;
        this.text = text;
        this.mTime = time;
        this.star = isStar;
        this.call = isCall;
        this.createTime = createTime;
    }

    public ShakeFile(File file) {
        this(-1, file, true, Util.getRecordTime(file.getAbsolutePath()), false,
                false);
    }

    public ShakeFile(int id, File file, boolean unread, int time,
            boolean isStar, boolean isCall) {
        this.id = id;
        this.unread = unread;
        this.mFile = file;
        this.mType = TYPE_3GP;
        this.mTime = time;
        this.createTime = file.lastModified();
        this.star = isStar;
        this.call = isCall;
    }

    public int getType() {
        return mType;
    }

    public void setType(int type) {
        this.mType = type;
    }

    public boolean moveFile(String newPath) {
        if (!mFile.exists()) {
            return false;
        }

        if (!mFile.renameTo(new File(newPath))) {
            return false;
        }

        mFile = new File(newPath);
        if (mFile == null) {
            return false;
        }

        return true;
    }

    public boolean delete() {
        return mFile.delete();
    }

    public String getFileName() {
        return (mFile.getName());
    }

    public int getState() {
        return mState;
    }

    public void setState(int state) {
        this.mState = state;
    }

    public File getFile() {
        return mFile;
    }

    public boolean isUnread() {
        return unread;
    }

    public void setUnread(boolean unread) {
        this.unread = unread;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String toString() {
        return "id = " + id + " name = " + mFile.getName() + " unread = "
                + unread + " type = " + mType;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isHead() {
        return isHead;
    }

    public void setHead(boolean isHead) {
        this.isHead = isHead;
    }

    public int getHeadType() {
        return headType;
    }

    public void setHeadType(int headType) {
        this.headType = headType;
    }

    public long getHeadTime() {
        return headTime;
    }

    public void setHeadTime(long headTime) {
        this.headTime = headTime;
    }

    public int getListType() {
        return listType;
    }

    public void setListType(int listType) {
        this.listType = listType;
    }

    public boolean isCollapsed() {
        return collapsed;
    }

    public void setCollapsed(boolean collapsed) {
        this.collapsed = collapsed;
    }

    public boolean toggleCollapsed() {
        boolean oldCollapsed = collapsed;
        collapsed = !collapsed;
        return oldCollapsed;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getTime() {
        return mTime;
    }

    public void setTime(int mTime) {
        this.mTime = mTime;
    }

    public boolean isStar() {
        return star;
    }

    public void setStar(boolean star) {
        this.star = star;
    }

    public boolean isCall() {
        return call;
    }

    public void setCall(boolean call) {
        this.call = call;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }
}
