package com.mengjingtech.easyrecorder;

import java.io.IOException;
import java.util.ArrayList;

import com.mengjingtech.lib.DebugUtil;
import com.mengjingtech.easyrecorder.R;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

public class ShakePlayerService extends Service implements
		MediaPlayer.OnCompletionListener {
	private static final boolean DEBUG = ShakeConst.DEBUG;
	private static final String TAG = "ShakePlayerService";

	public static final int AUDIO_SOURCE_NONE = 0;
	public static final int AUDIO_SOURCE_RECORD_ACTIVITY = 1;
	public static final int AUDIO_SOURCE_LIST_ACTIVITY = 2;
	private int mAudioSourceType = AUDIO_SOURCE_NONE;

	private static final String PLAY_CONTROL_ACTION = "com.mengjingtech.easyrecorder.PLAY_CONTROL_ACTION";

	// Update the play notification progress every 100ms.
	private static final long PLAY_NOTIFICATION_UPDATE_INTERVAL = 1000;

	// The id of the buttons in the playing notification.
	public static final String BUTTON_ID = "button_id";
	public static final int PLAY_PAUSE_BUTTON_ID = 0;
	public static final int STOP_BUTTON_ID = 1;
	public static final int CANCEL_BUTTON_ID = 2;

	private MediaPlayer mMediaPlayer;
	private ShakeFile mPlayFile = null;

	private PlayerBinder pb = new PlayerBinder();

	private Notification playNotification = null;
	private RemoteViews mRemoteViews = null;

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		if (DEBUG)
			Log.d(TAG, "onBind");
		return pb;
	}

	@Override
	public void onCompletion(MediaPlayer player) {
		if (mPlayFile != null) {
			mPlayFile.setState(ShakeFile.STATE_STOPED);
		}
		if (mPlayStateListenerList != null) {
			int size = mPlayStateListenerList.size();
			for (int i = size - 1; i > -1; i--) {
				PlayStateListener ps = mPlayStateListenerList.get(i);
				ps.onComplete();
			}
		}
	}

	@Override
	public void onCreate() {
		super.onCreate();
		mMediaPlayer = new MediaPlayer();
		mMediaPlayer.setOnCompletionListener(this);
	}

	public int onStartCommand(Intent intent, int flags, int startId) {
		if (intent != null) {
			String action = intent.getAction();

			if (DEBUG)
				Log.d(TAG, "onStartCommand" + action);

			if (PLAY_CONTROL_ACTION.equals(action)) {
				int buttonId = intent.getIntExtra(BUTTON_ID, 0);
				if (DEBUG)
					Log.d(TAG, "button_id = " + buttonId);
				if (buttonId == PLAY_PAUSE_BUTTON_ID) {
					if (isPlaying()) {
						pause();
					} else if (isPaused()) {
						resume();
					} else {
						start(mPlayFile, mAudioSourceType);
					}
				} else if (buttonId == STOP_BUTTON_ID) {
					stop();
				} else if (buttonId == CANCEL_BUTTON_ID) {
					stop();
					reset();
					NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
					nm.cancel(R.id.notify_in_playing_id);
				}
			}
		}
		return START_STICKY;
	}

	public void onDestroy() {
		if (DEBUG)
			Log.d(TAG, "onDestroy");
		super.onDestroy();
		mMediaPlayer.release();
	}

	class PlayerBinder extends Binder {
		public ShakePlayerService getService() {
			return ShakePlayerService.this;
		}
	}

	public void preparePlayback(ShakeFile sf) {
		if (DEBUG)
			Log.d(TAG, DebugUtil.getMethodName(new Exception()));
		reset();
		mPlayFile = sf;
		try {
			mMediaPlayer.setDataSource(sf.getFile().getAbsolutePath());
			mMediaPlayer.prepare();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		mPlayFile.setState(ShakeFile.STATE_PREPARED);
	}

	public void resume() {
		if (!isPaused()) {
			return;
		}

		if (DEBUG)
			Log.d(TAG, DebugUtil.getMethodName(new Exception()));
		mMediaPlayer.start();
		if (mPlayFile != null) {
			mPlayFile.setState(ShakeFile.STATE_PLAYING);
		}
		if (mPlayStateListenerList != null) {
			for (PlayStateListener ps : mPlayStateListenerList) {
				ps.onStartPlay();
			}
		}
	}

	public void start(ShakeFile sf, int audioSourceType) {
		if (DEBUG)
			Log.d(TAG, DebugUtil.getMethodName(new Exception()));
		mMediaPlayer.reset();
		preparePlayback(sf);
		start(audioSourceType);
	}
	
	public void start(ShakeFile sf, int audioSourceTypes, int progress) {
		start(audioSourceTypes);
	}

	private void start(int audioSourceType) {
		if (DEBUG)
			Log.d(TAG, DebugUtil.getMethodName(new Exception()));
		// set which activity started the audio playing
		mAudioSourceType = audioSourceType;
		mMediaPlayer.start();
		if (mPlayFile != null) {
			mPlayFile.setState(ShakeFile.STATE_PLAYING);
		}
		if (mPlayStateListenerList != null) {
			int size = mPlayStateListenerList.size();
			for (int i = size - 1; i > -1; i--) {
				PlayStateListener ps = mPlayStateListenerList.get(i);
				ps.onStartPlay();
			}
		}

		notifyInPlaying();
	}

	public void stop() {
		if (DEBUG)
			Log.d(TAG, DebugUtil.getMethodName(new Exception()));
		// mAudioSourceType = AUDIO_SOURCE_NONE;
		if (mMediaPlayer.isPlaying()) {
			mMediaPlayer.stop();
		}

		if (mPlayFile != null) {
			mPlayFile.setState(ShakeFile.STATE_STOPED);
		}

		if (mPlayStateListenerList != null) {
			int size = mPlayStateListenerList.size();
			for (int i = size - 1; i > -1; i--) {
				PlayStateListener ps = mPlayStateListenerList.get(i);
				ps.onStop();
			}
		}
	}

	private void reset() {
		if (DEBUG)
			Log.d(TAG, DebugUtil.getMethodName(new Exception()));
		mMediaPlayer.reset();
		if (mPlayFile != null) {
			mPlayFile.setState(ShakeFile.STATE_RESET);
		}

		if (mPlayStateListenerList != null) {
			int size = mPlayStateListenerList.size();
			for (int i = size - 1; i > -1; i--) {
				PlayStateListener ps = mPlayStateListenerList.get(i);
				ps.onReset();
			}
		}
	}

	public void pause() {
		if (DEBUG)
			Log.d(TAG, DebugUtil.getMethodName(new Exception()));
		mMediaPlayer.pause();
		if (mPlayFile != null) {
			mPlayFile.setState(ShakeFile.STATE_PAUSED);
		}
		if (mPlayStateListenerList != null) {
			int size = mPlayStateListenerList.size();
			for (int i = size - 1; i > -1; i--) {
				PlayStateListener ps = mPlayStateListenerList.get(i);
				ps.onPaused();
			}
		}
	}

	public void removeCurrentFile() {
		reset();
		handler.removeCallbacks(updatePlayingRunnable);
		NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		nm.cancel(R.id.notify_in_playing_id);
	}

	public int getCurrentPosition() {
		return mMediaPlayer.getCurrentPosition();
	}

	public int getDuration() {
		return mMediaPlayer.getDuration();
	}

	public boolean isStopped() {
		if (mPlayFile != null && mPlayFile.getState() == ShakeFile.STATE_STOPED) {
			return true;
		}
		return false;
	}

	public boolean isReset() {
		if (mPlayFile != null && mPlayFile.getState() == ShakeFile.STATE_RESET) {
			return true;
		}
		return false;
	}

	public boolean isPrepared() {
		if (mPlayFile != null
				&& mPlayFile.getState() == ShakeFile.STATE_PREPARED) {
			return true;
		}
		return false;
	}

	public boolean isPaused() {
		if (mPlayFile != null && mPlayFile.getState() == ShakeFile.STATE_PAUSED) {
			return true;
		}
		return false;
	}

	public boolean isPlaying() {
		if (mPlayFile != null
				&& mPlayFile.getState() == ShakeFile.STATE_PLAYING) {
			return true;
		}
		return false;
	}

	public void seekTo(int progress) {
		mMediaPlayer.seekTo(progress);
	}

	interface PlayStateListener {
		public void onComplete();

		public void onStop();

		public void onReset();

		public void onPaused();

		public void onStartPlay();
	}

	private ArrayList<PlayStateListener> mPlayStateListenerList = new ArrayList<PlayStateListener>();

	public void registerPlayStateListener(PlayStateListener playStateListener) {
		if (playStateListener != null
				&& !mPlayStateListenerList.contains(playStateListener)) {
			mPlayStateListenerList.add(playStateListener);
			// notify the current state to the new registered listener.
			notifyCurrentState(playStateListener);
		}
	}

	public void unregisterPlayStateListener(PlayStateListener playStateListener) {
		if (mPlayStateListenerList != null && playStateListener != null) {
			mPlayStateListenerList.remove(playStateListener);
		}
	}

	private void notifyCurrentState(PlayStateListener playStateListener) {
		int state = 0;
		if (mPlayFile != null) {
			state = mPlayFile.getState();
		}
		if (isPlaying()) {
			playStateListener.onStartPlay();
		} else if (isPaused()) {
			playStateListener.onPaused();
		} else {
			playStateListener.onStop();
		}
	}

	public int getAudioSourceType() {
		return mAudioSourceType;
	}

	public void setAudioSourceType(int mAudioSource) {
		this.mAudioSourceType = mAudioSource;
	}

	public ShakeFile getPlayFile() {
		return mPlayFile;
	}
	
	public void setPlayFile(ShakeFile sf) {
		mPlayFile = sf;
	}

	private Handler handler = new Handler();

	private Runnable updatePlayingRunnable = new Runnable() {
		@Override
		public void run() {
			// Update the playing progress.
			playNotification.contentView.setProgressBar(R.id.progress_bar,
					getDuration(), getCurrentPosition(), false);
			showNotification();

			if (isPlaying()) {
				handler.postDelayed(updatePlayingRunnable, PLAY_NOTIFICATION_UPDATE_INTERVAL);
			}
		}
	};

	public void notifyInPlaying() {
		NotificationCompat.Builder mBuilder = new Builder(this);
		if (mRemoteViews == null || playNotification == null) {
			mRemoteViews = new RemoteViews(getPackageName(),
					R.layout.playing_notification);

			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD) {
				mRemoteViews.setViewVisibility(R.id.notify_play_pause,
						View.GONE);
				mRemoteViews.setViewVisibility(R.id.notify_stop, View.GONE);
				mRemoteViews.setViewVisibility(R.id.notify_cancel, View.GONE);
			}

			if (isPlaying()) {
				mRemoteViews.setImageViewResource(R.id.notify_play_pause,
						R.drawable.pause_circle_light_blue_button);
			} else {
				mRemoteViews.setImageViewResource(R.id.notify_play_pause,
						R.drawable.play_circle_light_blue_button);
			}

			// stop button intent
			Intent buttonIntent = new Intent(this, ShakePlayerService.class);
			buttonIntent.setAction(PLAY_CONTROL_ACTION);
			buttonIntent.putExtra(BUTTON_ID, STOP_BUTTON_ID);
			PendingIntent stopIntent = PendingIntent.getService(this, 0,
					buttonIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			mRemoteViews.setOnClickPendingIntent(R.id.notify_stop, stopIntent);
			// play pause button intent
			buttonIntent.putExtra(BUTTON_ID, PLAY_PAUSE_BUTTON_ID);
			PendingIntent palypauseIntent = PendingIntent.getService(this, 1,
					buttonIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			mRemoteViews.setOnClickPendingIntent(R.id.notify_play_pause,
					palypauseIntent);
			// cancel button intent
			buttonIntent.putExtra(BUTTON_ID, CANCEL_BUTTON_ID);
			PendingIntent cancelIntent = PendingIntent.getService(this, 2,
					buttonIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			mRemoteViews.setOnClickPendingIntent(R.id.notify_cancel,
					cancelIntent);

			mRemoteViews.setProgressBar(R.id.progress_bar, 100, 0, false);

			Intent notificationIntent = new Intent(this,
					ShakeRecordListFullActivity.class);
//			if (mAudioSourceType == AUDIO_SOURCE_RECORD_ACTIVITY) {
//				notificationIntent = new Intent(this, ShakeRecordActivity.class);
//				notificationIntent.putExtra(ShakeRecordActivity.IS_PLAYING,
//						true);
//			}

			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_SINGLE_TOP);
			PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
					notificationIntent, 0);

			mBuilder.setContent(mRemoteViews).setContentIntent(contentIntent)
					.setWhen(System.currentTimeMillis())
					.setPriority(Notification.PRIORITY_DEFAULT)
					.setOngoing(true).setSmallIcon(R.drawable.logo);
			playNotification = mBuilder.build();
			playNotification.flags = Notification.FLAG_ONGOING_EVENT;
		} else {
//			Intent notificationIntent = new Intent(this,
//					ShakeRecordListFullActivity.class);
//			if (mAudioSourceType == AUDIO_SOURCE_RECORD_ACTIVITY) {
//				notificationIntent = new Intent(this, ShakeRecordActivity.class);
//				notificationIntent.putExtra(ShakeRecordActivity.IS_PLAYING,
//						true);
//			}
//
//			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
//					| Intent.FLAG_ACTIVITY_SINGLE_TOP);
//			playNotification.contentIntent = PendingIntent.getActivity(this, 0,
//					notificationIntent, 0);
		}
		
		mRemoteViews.setTextViewText(R.id.notify_name, mPlayFile.getFileName()
				.substring(0, mPlayFile.getFileName().length() - 4));

		registerPlayStateListener(mPlayStateListener);
	}

	private void showNotification() {
		NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		nm.notify(R.id.notify_in_playing_id, playNotification);
	}

	private PlayStateListener mPlayStateListener = new PlayStateListener() {
		@Override
		public void onComplete() {
			handler.removeCallbacks(updatePlayingRunnable);
			// reset progress bar
			playNotification.contentView.setProgressBar(R.id.progress_bar, 100,
					0, false);
			// set the play button.
			playNotification.contentView.setImageViewResource(
					R.id.notify_play_pause,
					R.drawable.play_circle_light_blue_button);
			showNotification();
		}

		@Override
		public void onStop() {
			// let the play notification update to stop state.
			// updatePlayingRunnable.run();
			handler.removeCallbacks(updatePlayingRunnable);
			playNotification.contentView.setImageViewResource(
					R.id.notify_play_pause,
					R.drawable.play_circle_light_blue_button);
			playNotification.contentView.setProgressBar(R.id.progress_bar,
					getDuration(), 0, false);
			showNotification();
			unregisterPlayStateListener(mPlayStateListener);
		}

		@Override
		public void onReset() {
		}

		@Override
		public void onPaused() {
			handler.removeCallbacks(updatePlayingRunnable);
			playNotification.contentView.setImageViewResource(
					R.id.notify_play_pause,
					R.drawable.play_circle_light_blue_button);
			showNotification();
		}

		@Override
		public void onStartPlay() {
			playNotification.contentView.setImageViewResource(
					R.id.notify_play_pause,
					R.drawable.pause_circle_light_blue_button);
			showNotification();
			handler.removeCallbacks(updatePlayingRunnable);
			handler.post(updatePlayingRunnable);
		}
	};
}
