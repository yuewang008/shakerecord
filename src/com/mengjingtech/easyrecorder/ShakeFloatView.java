package com.mengjingtech.easyrecorder;

import com.mengjingtech.easyrecorder.R;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class ShakeFloatView extends RelativeLayout {
    private static final boolean DEBUG = ShakeConst.DEBUG;
    private static final String TAG = "ShakeFloatView";
	private float mTouchX;
	private float mTouchY;
	private float x;
	private float y;
	private float mStartX;
	private float mStartY;
	private ImageView mImage = null;
	private OnClickListener mClickListener;
	private WindowManager windowManager = (WindowManager) getContext()
			.getApplicationContext().getSystemService(Context.WINDOW_SERVICE);

	private WindowManager.LayoutParams windowManagerParams = ((ShakeRecordApp) getContext()
			.getApplicationContext()).getWindowParams();
	
	private static ShakeFloatView mShakeFloatView  = null;
	
	public static ShakeFloatView getFloatView(Context context) {
	    if (mShakeFloatView == null) {
	        mShakeFloatView = new ShakeFloatView(context);
	    }
	    return mShakeFloatView;
	}

	public ShakeFloatView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

	private ShakeFloatView(Context context) {
		super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        mShakeFloatView = (ShakeFloatView) inflater.inflate(R.layout.float_view, null);
        mImage = (ImageView)mShakeFloatView.findViewById(R.id.recordButton);
	}
	

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// 获取到状态栏的高�??
		Rect frame = new Rect();
		getWindowVisibleDisplayFrame(frame);
		int statusBarHeight = frame.top;
		System.out.println("statusBarHeight:" + statusBarHeight);
		// 获取相对屏幕的坐标，即以屏幕左上角为原点
		x = event.getRawX();
		y = event.getRawY() - statusBarHeight; // statusBarHeight是系统状态栏的高�??
		Log.i("tag", "currX" + x + "====currY" + y);
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN: // 捕获手指触摸按下动作
			// 获取相对View的坐标，即以此View左上角为原点
			mTouchX = event.getX();
			mTouchY = event.getY();
			mStartX = x;
			mStartY = y;
			Log.i("tag", "startX" + mTouchX + "====startY" + mTouchY);
			break;
		case MotionEvent.ACTION_MOVE: // 捕获手指触摸移动动作
			updateViewPosition();
			break;
		case MotionEvent.ACTION_UP: // 捕获手指触摸离开动作
			updateViewPosition();
			mTouchX = mTouchY = 0;
			if ((x - mStartX) < 5 && (y - mStartY) < 5) {
				if (mClickListener != null) {
					mClickListener.onClick(this);
				}
			}
			break;
		}
		return true;
	}

	@Override
	public void setOnClickListener(OnClickListener l) {
		this.mClickListener = l;
	}

	private void updateViewPosition() {
		// 更新浮动窗口位置参数
		windowManagerParams.x = (int) (x - mTouchX);
		windowManagerParams.y = (int) (y - mTouchY);
		windowManager.updateViewLayout(this, windowManagerParams); // 刷新显示
	}
}
