package com.mengjingtech.easyrecorder;

public abstract class ShakeConst {
    public static final boolean DEBUG = false;

    public static final int KEY_POSITION = 1;
    
    public static final String PLAYER_SERVICE = "com.mengjingtech.easyrecorder.PLAYER_SERVICE";
    public static final String PACKAGE_NAME = "com.mengjingtech.easyrecorder";

    public static final String SUFFIX = ".aac";

    public static final String APP_NAME = "easyrecorder";
    public static final String REFRESH = "refresh"; // indicate if the record
                                                    // list need to be
                                                    // refreshed.

    public static final String PER_KEY_SHAKE_ENABLE = "enable";
    public static final String PER_KEY_LEFT_HAND = "left_hand";
    public static final String PER_KEY_NOTIFY_ENABLE = "notify_enable";
    public static final String PER_SYNC_FILE_DB_DONE = "sync_file_db_done";
    public static final String PER_FIRST_LAUNCH = "frist_launch";
    public static final String IS_LAUNCH_BY_SHAKE = "is_launch_by_shake";
    public static final String FLOAT_VIEW_X = "float_view_x";
    public static final String FLOAT_VIEW_Y = "float_view_y";
    
    public static final String EXTRA_MESSAGER="com.mengjingtech.easyrecorder.EXTRA_MESSAGER";
    
    public static final String ACTION_SYNC_FILE_DB_DONE = "com.mengjingtech.shake.ACTION_SYNC_FILE_DB_DONE";
    public static final String ACTION_SYNC_FILE_DB_BATCH = "com.mengjingtech.shake.ACTION_SYNC_FILE_DB_BATCH";

    public static final long SEEKBAR_UPDATE_INTERVAL = 50;
    
    public static final int DEF_LIMIT = 10;    // default items number to retrive
    public static final int DEF_OFFSET = 10;   // default item offset to retrive

    public interface ServiceMessage {
        // msg between service and activity
        final static int ACTIVITY_MESSENGER = 0;
        final static int OUTGOING_CALL_ACTIVE = 1;
        final static int INCOMING_CALL_ACTIVE = 2;
        final static int CALL_END = 4;
        final static int FINISH_RECORD_ACTIVITY = 5;

        final static int UPDATE_RECORD_TIME = 50;
        final static int UPDATE_ACTIVITY_RECORD_TIME = 51;

        // msg from shakelistener
        final static int SENSOR_SHAKE = 80;

        // msg from IntentService
        final static int SYNC_FILE_DB_DONE = 200;
        final static int SYNC_FILE_DB_BATCH = 201;
    }
}