package com.mengjingtech.easyrecorder;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.mengjingtech.lib.Util;

public class NotiListAdapter extends BaseAdapter {
	private ArrayList<ShakeNoti> mList;
	private Context context;
	private LayoutInflater inflater = null;

	public NotiListAdapter(Context context, ArrayList<ShakeNoti> list) {
		this.context = context;
		inflater = LayoutInflater.from(context);
		mList = list;
	}

	@Override
	public int getCount() {
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		ShakeNoti sn = mList.get(position);
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.noti_item, null);
			holder = new ViewHolder();
			holder.cb = (CheckBox) convertView.findViewById(R.id.noti_enable);
			holder.cb.setChecked(sn.isEnabled());
			holder.tv = (TextView) convertView.findViewById(R.id.noti_time);
			holder.tv.setText(Util.formatTimeToString(sn.getDate()));
			holder.cb.setOnClickListener(mOnClickListener);
			holder.cb.setTag(sn);
			holder.tv.setTag(sn);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
			holder.cb.setTag(sn);
			holder.cb.setChecked(sn.isEnabled());
			holder.tv.setTag(sn);
			holder.tv.setText(Util.formatTimeToString(sn.getDate()));
		}

		if (sn.isSelected()) {
			convertView.setBackgroundColor(context.getResources().getColor(
					R.color.selected_light_blue));
		} else {
			convertView.setBackgroundColor(context.getResources().getColor(
					R.color.bg_gray));
		}

		return convertView;
	}

	private OnClickListener mOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			ShakeNoti sn = (ShakeNoti) view.getTag();
			sn.setEnabled(((CheckBox) view).isChecked());
			ShakeNotiManager.getInstance(context).updateNoti(sn);
		}
	};

	class ViewHolder {
		TextView tv;
		CheckBox cb;
	}

	interface ViewClickListener {
		public void onViewClick(View view);
	}

}