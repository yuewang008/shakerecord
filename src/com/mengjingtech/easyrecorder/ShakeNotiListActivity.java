package com.mengjingtech.easyrecorder;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.mengjingtech.lib.CustomDialog;
import com.mengjingtech.easyrecorder.R;

public class ShakeNotiListActivity extends Activity {
	private static final boolean DEBUG = ShakeConst.DEBUG;
	private static final String TAG = "ShakeNotiListActivity";

	private static final int EDIT_TIME = 1;
	private static final int MAX_NOTI_NUM = 5;

	private ListView mListView;
	private ShakeNotiManager mShakeNotiManager;
	private ArrayList<ShakeNoti> mList;
	private NotiListAdapter mNotiListAdaptor;
	private Calendar c;
	private TextView mtvAddNoti;
	private int mEditPosition = -1;
	private TimePicker timePicker;
	private ArrayList<ShakeNoti> selectedItems = new ArrayList<ShakeNoti>();
	private Dialog notiDeleteDialog;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.noti_list);

		mListView = (ListView) findViewById(R.id.list_view);
		mShakeNotiManager = ShakeNotiManager
				.getInstance(getApplicationContext());
		mList = mShakeNotiManager.getShakeNotiList();
		mNotiListAdaptor = new NotiListAdapter(this, mList);
		mListView.setAdapter(mNotiListAdaptor);
		mListView.setOnItemClickListener(mOnItemClickListener);
		mListView.setOnItemLongClickListener(mLongClickListener);

		c = Calendar.getInstance();
		c.set(1970, 0, 1);

		timePicker = new TimePicker(this);
		timePicker.setIs24HourView(true);
	}

	@Override
	protected void onResume() {
		super.onResume();
		mNotiListAdaptor.notifyDataSetChanged();
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.clear();
		getMenuInflater().inflate(R.menu.menu_noti, menu);
		if (mList.size() >= MAX_NOTI_NUM) {
			menu.findItem(R.id.new_noti).setVisible(false);
		}

		// if some items are selected, show the delete action button.
		if (selectedItems.size() <= 0) {
			menu.findItem(R.id.delete_noti).setVisible(false);
		}
		return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	private OnItemLongClickListener mLongClickListener = new OnItemLongClickListener() {

		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view,
				int position, long id) {
			ShakeNoti sn = mList.get(position);
			if (sn.isSelected()) {
				sn.setSelected(false);
				mNotiListAdaptor.notifyDataSetChanged();
				int size = selectedItems.size();
				for (int i = 0; i < size; i++) {
					if (selectedItems.get(i) == sn) {
						selectedItems.remove(i);
						break;
					}
				}
				if (selectedItems.size() == 0) {
					invalidateOptionsMenu();
				}
			} else {
				sn.setSelected(true);
				mNotiListAdaptor.notifyDataSetChanged();
				selectedItems.add(sn);
				if (selectedItems.size() == 1) {
					invalidateOptionsMenu();
				}
			}
			return true;
		}
	};

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.delete_noti:
			shownotiDeleteDialog();
			break;
		case R.id.new_noti:
			showTimepicker(12, 0);
			mEditPosition = -1;
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void shownotiDeleteDialog() {
		View dialogView = View.inflate(this, R.layout.filelist_delete_dialog,
				null);
		notiDeleteDialog = new CustomDialog(this, dialogView,
				R.style.mengjingtech_dialog);

		TextView dialog_button_ok = (TextView) dialogView
				.findViewById(R.id.button_ok);
		dialog_button_ok.setClickable(true);
		dialog_button_ok.setOnClickListener(mNotiDeleteClickListener);
		dialog_button_ok.setEnabled(true);

		TextView dialog_button_cancel = (TextView) dialogView
				.findViewById(R.id.button_cancel);
		dialog_button_cancel.setClickable(true);
		dialog_button_cancel.setOnClickListener(mNotiDeleteClickListener);
		dialog_button_cancel.setEnabled(true);

		notiDeleteDialog.show();
	}

	private OnClickListener mNotiDeleteClickListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			switch (arg0.getId()) {
			case R.id.button_cancel:
				break;
			case R.id.button_ok:
				mShakeNotiManager.deleteNotis(selectedItems);
				selectedItems.clear();
				mNotiListAdaptor.notifyDataSetChanged();
				break;
			default:
				break;
			}
			notiDeleteDialog.dismiss();
		}
	};

	private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent, View v, int position,
				long id) {
			if (selectedItems.size() > 0) {

			} else {
				ShakeNoti sn = mList.get(position);
				Calendar cal = Calendar.getInstance();
				cal.setTime(sn.getDate());
				int hour = cal.get(Calendar.HOUR_OF_DAY);
				int minute = cal.get(Calendar.MINUTE);
				showTimepicker(hour, minute);
				mEditPosition = position;
			}
		}
	};

	TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			c.set(Calendar.HOUR_OF_DAY, hourOfDay);
			c.set(Calendar.MINUTE, minute);
			c.set(Calendar.SECOND, 0);
			c.set(Calendar.MILLISECOND, 0);
			mHandler.sendEmptyMessage(EDIT_TIME);
		}
	};

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case EDIT_TIME:
				if (mEditPosition >= 0) {
					ShakeNoti sn = mList.get(mEditPosition);
					sn.setDate(c.getTime());
					mShakeNotiManager.updateNoti(sn);
				} else {
					ShakeNoti sn = new ShakeNoti();
					sn.setDate(c.getTime());
					mShakeNotiManager.addNoti(sn);
				}
				mNotiListAdaptor.notifyDataSetChanged();
				break;
			}
		}
	};

	private AlertDialog al = null;

	private void showTimepicker(int hour, int minute) {
		timePicker.setCurrentHour(hour);
		timePicker.setCurrentMinute(minute);
		if (al != null) {
			al.show();
		} else {
			al = new AlertDialog.Builder(this)
					.setTitle(R.string.set_time)
					.setPositiveButton(android.R.string.ok,
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									c.set(Calendar.HOUR_OF_DAY,
											timePicker.getCurrentHour());
									c.set(Calendar.MINUTE,
											timePicker.getCurrentMinute());
									c.set(Calendar.SECOND, 0);
									c.set(Calendar.MILLISECOND, 0);
									if (DEBUG)
										Log.d(TAG,
												timePicker.getCurrentHour()
														+ ":"
														+ timePicker
																.getCurrentMinute());
									mHandler.sendEmptyMessage(EDIT_TIME);
								}
							})
					.setNegativeButton(android.R.string.cancel,
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
								}
							}).setView(timePicker).show();
		}
	}
}
