package com.mengjingtech.easyrecorder;

import java.io.File;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mengjingtech.easyrecorder.ShakeFileManager.DataChangeListener;
import com.mengjingtech.lib.Util;
import com.mengjingtech.easyrecorder.R;

public class ShakeFileAdapter extends BaseAdapter {
    private static final boolean DEBUG = ShakeConst.DEBUG;
    private static final String TAG = "ShakeFileAdter";

    public Context mContext;
    public ArrayList<ShakeFile> mList;
    public ArrayList<ShakeFile> mHeadList;
    private ViewClickListener viewClickListener = null;
    private LastItemShowListener lastItemShowListener = null;
    private int mProgress = 0, mMax = 1;
    private int mLastPosition = 0;

    private static final int[] resArray = { R.string.today, R.string.yesterday,
            R.string.two_days_ago, R.string.a_week_ago };

    private static final boolean[] collapsed = new boolean[ShakeFile.LIST_TYPE_TOTAL];

    public int getCount() {
        return mList.size();
    }

    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return mList.get(arg0);
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    class ItemViewHolder {
        TextView tvName, tvTime, tvDuration, tvDate, tvCount;
        ImageView ivStar, ivCall;
        RoundProgressBar rpbPlay;
        View vHead, vItem;
    }

    private void showItem(View view, boolean isItem) {
    	if (DEBUG) Log.d(TAG, "showItem isItem:" + isItem);
        ItemViewHolder holder = (ItemViewHolder) view
                .getTag(R.id.item_view_holder);
        if (isItem) {
            holder.vItem.setVisibility(View.VISIBLE);
            holder.vHead.setVisibility(View.GONE);
        } else {
            holder.vHead.setVisibility(View.VISIBLE);
            holder.vItem.setVisibility(View.GONE);
        }
    }

    public View getView(int position, View convertView, ViewGroup arg2) {
        if (DEBUG)
            Log.d(TAG, "getView position = " + position);
        // handle the last item view to load next 10 items.
        if (position == mList.size() - 1 && position > mLastPosition) {
            if (lastItemShowListener != null) {
            	if (DEBUG) Log.d(TAG, "last Item shown");
                lastItemShowListener.onLastItemShow();
                mLastPosition = position;
            }
        }

        ShakeFile sf = mList.get(position);
        ItemViewHolder holder;

        if (convertView == null) {
            convertView = View.inflate(mContext, R.layout.record_item, null);
            holder = new ItemViewHolder();
            holder.vHead = convertView.findViewById(R.id.list_head);
            holder.vItem = convertView.findViewById(R.id.shakefile_item);
            holder.tvDate = (TextView) convertView.findViewById(R.id.date);
            holder.tvCount = (TextView) convertView.findViewById(R.id.count);
            holder.tvName = (TextView) convertView.findViewById(R.id.file_name);
            holder.tvTime = (TextView) convertView.findViewById(R.id.file_time);
            holder.tvDuration = (TextView) convertView
                    .findViewById(R.id.file_length);
            holder.ivStar = (ImageView) convertView
                    .findViewById(R.id.star_button);
            holder.ivCall = (ImageView) convertView
                    .findViewById(R.id.call_status);
            holder.rpbPlay = (RoundProgressBar) convertView
                    .findViewById(R.id.img_play_stop);
            convertView.setTag(R.id.item_view_holder, holder);
        } else {
            holder = (ItemViewHolder) convertView.getTag(R.id.item_view_holder);
        }

        if (sf.isHead()) {
            showItem(convertView, false);
            if (ShakeFileManager.getInstance(mContext).listCount[sf
                    .getHeadType()] == 0) {
                holder.vHead.setVisibility(View.GONE);
            } else {
                holder.vHead.setVisibility(View.VISIBLE);
            }
            holder.tvDate.setText(resArray[sf.getHeadType()]);
            holder.vHead.setClickable(false);
            holder.vHead.setLongClickable(false);
        } else {
            showItem(convertView, true);
            if (sf.isSelected()) {
            	if (DEBUG) Log.d(TAG, "isSelected:true");
                holder.vItem.setBackgroundColor(mContext.getResources()
                        .getColor(R.color.selected_light_blue));
            } else {
                holder.vItem.setBackgroundColor(mContext.getResources()
                        .getColor(R.color.bg_gray));
            }

            holder.rpbPlay.setClickable(true);
            holder.rpbPlay.setOnClickListener(mPlayOnClickListener);

            int imageRes = sf.isStar() ? R.drawable.ic_btn_star_on
                    : R.drawable.ic_btn_star_off;
            holder.ivStar.setImageResource(imageRes);
            holder.ivStar.setOnClickListener(mStarOnClickListener);
            holder.ivStar.setTag(R.id.position, Integer.valueOf(position));

            if (sf.isCall()) {
                holder.ivCall.setVisibility(View.VISIBLE);
            } else {
                holder.ivCall.setVisibility(View.INVISIBLE);
            }

            holder.vItem.setLongClickable(true);
            holder.vItem.setOnLongClickListener(mItemOnLongClickListener);
            holder.vItem.setClickable(true);
            holder.vItem.setOnClickListener(mItemClickListener);

            File f = sf.getFile();
            holder.tvName.setText(f.getName().substring(0,
                    f.getName().length() - 4));
            if (sf.isUnread()) {
                holder.tvName.setTypeface(holder.tvName.getTypeface(),
                        Typeface.BOLD);
            } else {
                holder.tvName.setTypeface(holder.tvName.getTypeface(),
                        Typeface.NORMAL);
            }

            holder.tvDuration.setText(Util.getStandardTime(sf.getTime()));

            holder.tvTime.setText(Util.getLocaleDateTime(sf.getCreateTime()));

            if (sf.getState() == ShakeFile.STATE_STOPED
                    || sf.getState() == ShakeFile.STATE_RESET
                    || sf.getState() == ShakeFile.STATE_PREPARED) {
                holder.rpbPlay
                        .setBackgroundResource(R.drawable.play_circle_light_blue_button);
                holder.rpbPlay.setProgress(0);
            } else if (sf.getState() == ShakeFile.STATE_PAUSED) {
                holder.rpbPlay
                        .setBackgroundResource(R.drawable.play_circle_light_blue_button);
                holder.rpbPlay.setMax(mMax);
                holder.rpbPlay.setProgress(mProgress);
            } else if (sf.getState() == ShakeFile.STATE_PLAYING) {
                if (DEBUG)
                    Log.d(TAG, "update playing progressbar mMax = " + mMax
                            + " mProgress = " + mProgress);
                holder.rpbPlay
                        .setBackgroundResource(R.drawable.pause_circle_light_blue_button);
                holder.rpbPlay.setMax(mMax);
                holder.rpbPlay.setProgress(mProgress);
            }
            holder.rpbPlay.setTag(R.id.position, Integer.valueOf(position));
        }
        holder.vItem.setTag(R.id.position, Integer.valueOf(position));
        return convertView;
    }

    private void setVisible(ViewGroup vg, int visibility) {
        int viewCount = vg.getChildCount();
        for (int i = 0; i < viewCount; i++) {
            vg.getChildAt(i).setVisibility(visibility);
        }
        vg.setVisibility(visibility);
    }

    interface ViewClickListener {
        public void onViewClick(View view);

        public void onViewLongClick(View view);
    }

    public void setOnViewClickListener(ViewClickListener listener) {
        viewClickListener = listener;
    }

    interface LastItemShowListener {
        public void onLastItemShow();
    }

    public void setLastItemShowListener(LastItemShowListener listener) {
        lastItemShowListener = listener;
    }

    private View.OnClickListener mStarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View arg0) {
            if (viewClickListener != null) {
                viewClickListener.onViewClick(arg0);
            } else {
                Log.e(TAG, "viewClickListener is null!");
            }
        }
    };

    private View.OnClickListener mPlayOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View arg0) {
            if (viewClickListener != null) {
                viewClickListener.onViewClick(arg0);
            } else {
                Log.e(TAG, "viewClickListener is null!");
            }
        }
    };

    private View.OnLongClickListener mItemOnLongClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            if (viewClickListener != null) {
                viewClickListener.onViewLongClick(v);
            } else {
                Log.e(TAG, "viewClickListener is null!");
            }
            return true;
        }
    };

    private View.OnClickListener mItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (viewClickListener != null) {
                viewClickListener.onViewClick(v);
            } else {
                Log.e(TAG, "viewClickListener is null!");
            }
        }
    };

    public ShakeFileAdapter(Context context, ShakeFileManager sm) {
        this.mContext = context;
        mList = new ArrayList<ShakeFile>();
    }

    public void setProgress(int progress) {
        mProgress = progress;
    }

    public void setMax(int max) {
        mMax = max;
    }

    public void setList(ArrayList<ShakeFile> list) {
        mList = list;
        notifyDataSetChanged();
    }

    public void showStarred(boolean starred) {
        notifyDataSetChanged();
    }
}
