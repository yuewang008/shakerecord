package com.mengjingtech.easyrecorder;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.WindowManager;

import com.mengjingtech.lib.Util;

public class ShakeRecordApp extends Application {
	private static final boolean DEBUG = ShakeConst.DEBUG;
	private static final String TAG = "ShakeRecordApp";

	private WindowManager.LayoutParams windowParams = new WindowManager.LayoutParams();
	private SharedPreferences mSharedPreferences;

	public WindowManager.LayoutParams getWindowParams() {
		return windowParams;
	}

	@Override
	public void onCreate() {
		super.onCreate();

		Util.initialFolders(ShakeConst.APP_NAME);

		IntentFilter filter = new IntentFilter(Intent.ACTION_TIME_TICK);
		registerReceiver(mReceiver, filter);

		Intent i = new Intent(getApplicationContext(), ShakeService.class);
		startService(i);

		mSharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(this);

		if (!mSharedPreferences.getBoolean(ShakeConst.PER_SYNC_FILE_DB_DONE,
				false)) {
			Intent syncIntent = new Intent();
			syncIntent.setAction(ShakeIntentService.ACTION_SYNC_FILE_DB);
			syncIntent.putExtra(ShakeConst.EXTRA_MESSAGER, new Messenger(
					handler));
			startService(syncIntent);
		}
	}

	BroadcastReceiver mReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null) {
				boolean isServiceRunning = false;

				if (intent.getAction().equals(Intent.ACTION_TIME_TICK)) {
					// check if the shake service is running, start the service
					// if not.
					ActivityManager manager = (ActivityManager) getApplicationContext()
							.getSystemService(Context.ACTIVITY_SERVICE);
					for (RunningServiceInfo service : manager
							.getRunningServices(Integer.MAX_VALUE)) {
						if ("com.mengjingtech.easyrecorder.ShakeService"
								.equals(service.service.getClassName())) {
							isServiceRunning = true;
						}
					}
					if (!isServiceRunning) {
						Intent i = new Intent(context, ShakeService.class);
						context.startService(i);
					}
				}
			}
		}
	};

	// handle incoming message.
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			LocalBroadcastManager lbm = LocalBroadcastManager
					.getInstance(ShakeRecordApp.this);
			Intent intent = new Intent();
			switch (msg.what) {
			case ShakeConst.ServiceMessage.SYNC_FILE_DB_DONE:
				if (DEBUG)
					Log.d(TAG, "send ACTION_SYNC_FILE_DB_BATCH");
				// update the current file db sync status.
				SharedPreferences.Editor editor = mSharedPreferences.edit();
				editor.putBoolean(ShakeConst.PER_SYNC_FILE_DB_DONE, true);
				editor.commit();

				// local broadcast intent to notify the sync file db done.
				intent.setAction(ShakeConst.ACTION_SYNC_FILE_DB_DONE);
				lbm.sendBroadcast(intent);
				break;
			case ShakeConst.ServiceMessage.SYNC_FILE_DB_BATCH:
				if (DEBUG)
					Log.d(TAG, "send ACTION_SYNC_FILE_DB_BATCH");
				// local broadcast intent to notify the sync file db done.
				intent.setAction(ShakeConst.ACTION_SYNC_FILE_DB_BATCH);
				lbm.sendBroadcast(intent);
				break;
			default:
				break;
			}
		}
	};
}
