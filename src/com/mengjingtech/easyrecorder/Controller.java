package com.mengjingtech.easyrecorder;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;

public class Controller extends com.mengjingtech.lib.ControllerBase {
    static final String TAG = "MedController";
    final static boolean DEBUG = ShakeConst.DEBUG;

    /* add new action here */
    public static final int USER_DEFINE_EVENT           = 1000;
    public static final int LOAD_MORE_RECORD            = USER_DEFINE_EVENT + 1;
    public static final int STOP_RECORDING              = USER_DEFINE_EVENT + 2;
    public static final int LOAD_MORE_RECORD_QUERY      = USER_DEFINE_EVENT + 3;
    public static final int LOAD_MORE_RECORD_STARRED    = USER_DEFINE_EVENT + 4;
    

    public static final int DUMP                        = USER_DEFINE_EVENT + 3000;

    // static MedController mController = new MedController();
    static Controller mController = null;

    public Controller(Context context) {
        init(context);
        mController = this;
    }

    public static Controller getInstance(Context context) {
        if (mController == null) {
            mController = new Controller(context);
        }

        return mController;
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
        case USER_DEFINE_EVENT:
        case LOAD_MORE_RECORD:
            handleLoadMoreRecord(msg);
            break;
        case LOAD_MORE_RECORD_QUERY:
            handleLoadMoreRecordQuery(msg);
            break;
        case LOAD_MORE_RECORD_STARRED:
            handleLoadMoreRecordStarred(msg);
            break;
        case STOP_RECORDING:
            handleStopRecording(msg);
            break;
        default:
            break;
        }
    }

    public void loadMoreRecord(int limit, int offset) {
        Bundle obj = new Bundle();
        obj.putInt("limit", limit);
        obj.putInt("offset", offset);
        sendRequest(LOAD_MORE_RECORD, obj);
    }

    public void loadMoreRecord(int limit, int offset, boolean starred) {
        Bundle obj = new Bundle();
        obj.putInt("limit", limit);
        obj.putInt("offset", offset);
        obj.putBoolean("starred", starred);
        sendRequest(LOAD_MORE_RECORD_STARRED, obj);
    }
    
    public void loadMoreRecord(int limit, int offset, String query) {
        Bundle obj = new Bundle();
        obj.putInt("limit", limit);
        obj.putInt("offset", offset);
        obj.putString("query", query);
        sendRequest(LOAD_MORE_RECORD_QUERY, obj);
    }
    
    public void stopRecording(ShakeService service) {
        sendRequest(STOP_RECORDING, service);
    }
    
    void handleStopRecording(Message msg) {
        ShakeService service = (ShakeService) msg.obj;
        service.stopRecording();
        sendMessageToUI(msg.what, 0, 0, null);
    }
    
    void handleLoadMoreRecordStarred(Message msg) {
        Bundle obj = (Bundle) msg.obj;
        int limit = obj.getInt("limit", ShakeConst.DEF_LIMIT);
        int offset = obj.getInt("offset", ShakeConst.DEF_OFFSET);
        boolean starred = obj.getBoolean("starred", true);
        ArrayList<ShakeFile> list = ShakeFileManager.getInstance(mContext).getMoreRecord(limit, offset, starred);

        if (list != null && list.size() >= 0) {
            sendMessageToUI(msg.what, 0, 0, list);
        } else {
            sendMessageToUI(msg.what, -1, 0, null);
        }
    }
    
    void handleLoadMoreRecordQuery(Message msg) {
        Bundle obj = (Bundle) msg.obj;
        int limit = obj.getInt("limit", ShakeConst.DEF_LIMIT);
        int offset = obj.getInt("offset", ShakeConst.DEF_OFFSET);
        String query = obj.getString("query");
        ArrayList<ShakeFile> list = ShakeFileManager.getInstance(mContext).getMoreRecord(limit, offset, query);

        if (list != null && list.size() >= 0) {
            sendMessageToUI(msg.what, 0, 0, list);
        } else {
            sendMessageToUI(msg.what, -1, 0, null);
        }
    }

    void handleLoadMoreRecord(Message msg) {
        Bundle obj = (Bundle) msg.obj;
        int limit = obj.getInt("limit", ShakeConst.DEF_LIMIT);
        int offset = obj.getInt("offset", ShakeConst.DEF_OFFSET);
        ArrayList<ShakeFile> list = ShakeFileManager.getInstance(mContext).getMoreRecord(limit, offset);

        if (list != null && list.size() >= 0) {
            sendMessageToUI(msg.what, 0, 0, list);
        } else {
            sendMessageToUI(msg.what, -1, 0, null);
        }
    }
 }