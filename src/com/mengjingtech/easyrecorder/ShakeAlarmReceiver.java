package com.mengjingtech.easyrecorder;

import com.mengjingtech.easyrecorder.R;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;

public class ShakeAlarmReceiver extends BroadcastReceiver {
    private static final boolean DEBUG = ShakeConst.DEBUG;
    private static final String TAG = "ShakeAlarmReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        ShakeFileManager mShakeFileManager = ShakeFileManager.getInstance(context);
//        if (mShakeFileManager.hasPendingRedording());
        showNotification(context);

        // send intent to service to notify the noti time change.
        Intent serviceIntent = new Intent(context, ShakeService.class);
        serviceIntent.setAction(ShakeService.ACTION_NOTI_TIMER_FIRED);
        context.startService(serviceIntent);
    }

    private void showNotification(Context context) {
        if (DEBUG) Log.d(TAG, "alarm fired!");
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        
        Resources res = context.getResources();
        String notiString = null;
        notiString = res.getString(R.string.pending_record);
        
        Intent notificationIntent = new Intent(context, ShakeRecordListFullActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent contentItent = PendingIntent.getActivity(context, 0,
                notificationIntent, 0);
        
        @SuppressWarnings("deprecation")
        Notification notification = new Notification(R.drawable.play,
                notiString, System.currentTimeMillis());
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults = Notification.DEFAULT_ALL;
        
        CharSequence contentTitle = res.getString(R.string.app_name);
        CharSequence contentText = notiString;
        notification.setLatestEventInfo(context, contentTitle, contentText,
                contentItent);
        notificationManager.notify((String)contentTitle, R.id.notify_id, notification);
    }
}
