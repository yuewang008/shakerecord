package com.mengjingtech.easyrecorder;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.RemoteException;
import android.util.Log;

public class ShakeMonitorService extends Service {
    private static final boolean DEBUG = ShakeConst.DEBUG;
    private static final String TAG = "ShakeMonitorService";

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        bindToService();
    }

    private void bindToService() {
        Intent intent = new Intent("com.mengjingtech.easyrecorder.CORE_SERVICE");
        intent.setPackage("com.mengjingtech.easyrecorder");
        bindService(intent, mServiceConnection, BIND_AUTO_CREATE);
    }

    private ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName comp, IBinder binder) {
            if (DEBUG)
                Log.d(TAG, "onServiceConnected");
            try {
                binder.linkToDeath(mDeathRecipient, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName comp) {
            if (DEBUG)
                Log.d(TAG, "onServiceDisconnected");
            bindToService();
        }
    };

    DeathRecipient mDeathRecipient = new DeathRecipient() {
        @Override
        public void binderDied() {
            bindToService();
        }
    };

    @Override
    public void onDestroy() {
        unbindService(mServiceConnection);
        super.onDestroy();
    }

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return START_STICKY;
	}
}
