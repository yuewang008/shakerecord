package com.mengjingtech.easyrecorder;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class ShakeDBHelper extends SQLiteOpenHelper {
    private static final boolean DEBUG = ShakeConst.DEBUG;
    private static final String TAG = "ShakeDBHelper";

    public  static final String DBNAME = "seven_magic";
    public static final String TABLE_NAME = "shake_file";
    public static final String TABLE_NOTI_NAME = "shake_noti";

    public static final String FIELD_ID = "id";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_UNREAD = "unread";
    public static final String FIELD_TEXT = "text";
    public static final String FIELD_TIME = "time";
    public static final String FIELD_CTIME = "ctime";
    public static final String FIELD_STAR = "star";
    public static final String FIELD_CALL = "call";
    public static final String FIELD_TYPE = "type";
    
    public static String FIELD_NOTI_TIME = "noti_time";
    public static String FIELD_NOTI_ENABLE = "noti_enable";
    
    private static final String CREATE_FILE_TABLE = "CREATE TABLE " + 
            TABLE_NAME + " ( " +
            FIELD_ID +  " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            FIELD_NAME + " VARCHAR(50), " +
            FIELD_UNREAD + " SMALLINT, " + 
            FIELD_STAR + " SMALLINT, " +
            FIELD_CALL + " SMALLINT, " +
            FIELD_TEXT + " VARCHAR(300), " +
            FIELD_TYPE + " SMALLINT DEFAULT '1', " +
            FIELD_TIME + " INTEGER, " +
            FIELD_CTIME + " INTEGER " +
            "); ";
            
    private static final String CREATE_NOTI_TABLE = "CREATE TABLE " +
            TABLE_NOTI_NAME + " ( " +
            FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            FIELD_NOTI_TIME + " VARCHAR, " +
            FIELD_NOTI_ENABLE + " SMALLINT); ";

    public ShakeDBHelper(Context context, String name, CursorFactory factory,
            int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_FILE_TABLE);
        db.execSQL(CREATE_NOTI_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF IT EXISTS" + TABLE_NAME);
        db.execSQL("DROP TABLE IF IT EXISTS" + TABLE_NOTI_NAME);
        onCreate(db);
    }

}
