/*
 * Copyright (C) 2013 Motorola Mobility Inc.
 * All Rights Reserved.
 * Motorola Mobility Confidential Restricted.
 */
package com.mengjingtech.easyrecorder;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

public class navigator extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sp.edit();

        boolean fristload = sp.getBoolean(ShakeConst.PER_FIRST_LAUNCH, true);
        boolean isLaunchByShake = getIntent().getBooleanExtra(ShakeConst.IS_LAUNCH_BY_SHAKE, true);

        Intent intent;
        // first load to launch the training slides
        if (fristload) {
            intent = new Intent();
            intent.setClass(this, ScrollLayoutActivity.class);
            startActivityForResult(intent, 0);
            editor.putBoolean(ShakeConst.PER_FIRST_LAUNCH, false).commit();
        } else {
            intent = new Intent();
            intent.setClass(this, ShakeRecordActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.putExtra(ShakeRecordActivity.IS_PLAYING,
            		!isLaunchByShake);
            startActivity(intent);
        }
        finish();
    }
}
