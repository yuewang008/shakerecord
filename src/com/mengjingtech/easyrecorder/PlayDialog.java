package com.mengjingtech.easyrecorder;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.mengjingtech.easyrecorder.ShakePlayerService.PlayStateListener;
import com.mengjingtech.lib.Util;

public class PlayDialog extends Dialog {
    private ImageView ivPlay, ivStop, ivStar, ivEdit;
    private TextView tvCurrentPosition, tvFileName, tvFileDuration;
    private EditText etFileName;
    private SeekBar seekbar;
    private ShakeFile mShakeFile;
    private final Handler mHandler = new Handler();
    private Context mContext;

    private ShakePlayerService playerService;

    public PlayDialog(Context context, ShakePlayerService playService,
            ShakeFile sf) {
        super(context, R.style.mengjingtech_dialog);

        mContext = context;
        playerService = playService;
        mShakeFile = sf;
        // set content
        View view = View.inflate(context, R.layout.play_dialog, null);
        setContentView(view);

        // set window params
        Window window = getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        // set width,height by density and gravity
        params.gravity = Gravity.CENTER;
        window.setAttributes(params);

        tvFileName = (TextView) view.findViewById(R.id.text_file);
        etFileName = (EditText) view.findViewById(R.id.edit_file);
        tvFileName.setText(mShakeFile.getFileName().substring(0,
                mShakeFile.getFileName().length() - 4));

        tvFileDuration = (TextView) view.findViewById(R.id.text_duration);
        tvCurrentPosition = (TextView) view
                .findViewById(R.id.text_current_position);

        ivPlay = (ImageView) view.findViewById(R.id.play_button);
        ivPlay.setOnClickListener(mClickClistener);

        ivStop = (ImageView) view.findViewById(R.id.stop_button);
        ivStop.setOnClickListener(mClickClistener);

        ivEdit = (ImageView) view.findViewById(R.id.edit_save_button);
        ivEdit.setOnClickListener(mClickClistener);

        ivStar = (ImageView) view.findViewById(R.id.star_button);
        ivStar.setOnClickListener(mClickClistener);
        int imageRes = mShakeFile.isStar() ? R.drawable.ic_btn_star_on
                : R.drawable.ic_btn_star_off;
        ivStar.setImageResource(imageRes);

        seekbar = (SeekBar) view.findViewById(R.id.seek_bar);
        seekbar.setOnSeekBarChangeListener(mOnSeekBarChangeListener);

        initPlayState();
    }

    private void initPlayState() {
        if (mShakeFile.getState() == ShakeFile.STATE_PLAYING) {
            ivPlay.setBackgroundResource(R.drawable.pause_circle_light_blue_button);
        }
        if (mShakeFile != playerService.getPlayFile()) {
            playerService.preparePlayback(mShakeFile);
        }
        seekbar.setMax(playerService.getDuration());
        tvFileDuration.setText(Util.getStandardTime(playerService.getPlayFile()
                .getTime()));
        
        playerService.registerPlayStateListener(mPlayStateListener);
    }

    private void playback() {
        if (playerService.isPaused()) {
            playerService.resume();
        } else {
            playerService.start(mShakeFile, ShakePlayerService.AUDIO_SOURCE_LIST_ACTIVITY);
        }
    }

    Runnable updateThread = new Runnable() {
        public void run() {
            seekbar.setProgress(playerService.getCurrentPosition());
            mHandler.postDelayed(updateThread,
                    ShakeConst.SEEKBAR_UPDATE_INTERVAL);
        }
    };

    private View.OnClickListener mClickClistener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
            case R.id.play_button:
                if (playerService.isPlaying()) {
                    playerService.pause();
                } else {
                    playback();
                }
                break;
            case R.id.stop_button:
                if (mShakeFile.getState() == ShakeFile.STATE_PLAYING
                        || mShakeFile.getState() == ShakeFile.STATE_PAUSED) {
                    playerService.stop();
                }
                break;
            case R.id.star_button:
                mShakeFile.setStar(!mShakeFile.isStar());
                ShakeFileManager.getInstance(mContext).updateFile(mShakeFile);
                int imageRes = mShakeFile.isStar() ? R.drawable.ic_btn_star_on
                        : R.drawable.ic_btn_star_off;
                ((ImageView) v).setImageResource(imageRes);
                break;
            case R.id.edit_save_button:
                if (etFileName.getVisibility() != View.VISIBLE) {
                    etFileName.setText(tvFileName.getText());
                    etFileName.setVisibility(View.VISIBLE);
                    tvFileName.setVisibility(View.GONE);
                    etFileName.setEnabled(true);
                    etFileName.requestFocus();
                    ivEdit.setImageResource(R.drawable.ic_action_save);
                } else {
                    String fileName = etFileName.getText().toString();
                    if (fileName.length() > 0
                            && mShakeFile.moveFile(Util.appSaveDir + fileName
                                    + ShakeConst.SUFFIX)) {
                        ShakeFileManager.getInstance(mContext).updateFile(
                                mShakeFile);
                        tvFileName.setText(fileName);
                    }
                    etFileName.setVisibility(View.GONE);
                    tvFileName.setVisibility(View.VISIBLE);
                    etFileName.clearFocus();

                    ivEdit.setImageResource(R.drawable.ic_action_edit);
                }

                break;
            }
        }
    };

    private PlayStateListener mPlayStateListener = new PlayStateListener() {
        @Override
        public void onComplete() {
            mHandler.removeCallbacks(updateThread);
            ivPlay.setBackgroundResource(R.drawable.play_circle_light_blue_button);
            seekbar.setProgress(0);
        }

        @Override
        public void onStop() {
            mHandler.removeCallbacks(updateThread);
            ivPlay.setBackgroundResource(R.drawable.play_circle_light_blue_button);
            seekbar.setProgress(0);
        }
        
        @Override
        public void onReset() {
        }

        @Override
        public void onPaused() {
            mHandler.removeCallbacks(updateThread);
            ivPlay.setBackgroundResource(R.drawable.play_circle_light_blue_button);
            seekbar.setProgress(playerService.getCurrentPosition());
        }

        @Override
        public void onStartPlay() {
            ivPlay.setBackgroundResource(R.drawable.pause_circle_light_blue_button);
            mHandler.removeCallbacks(updateThread);
            mHandler.post(updateThread);
        }
    };

    private OnSeekBarChangeListener mOnSeekBarChangeListener = new OnSeekBarChangeListener() {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress,
                boolean fromUser) {
            if (fromUser) {
                playerService.seekTo(progress);
            }
            tvCurrentPosition.setText(Util.getStandardTime(progress / 1000));
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    @Override
    protected void onStop() {
//        if (mShakeFile.getState() == ShakeFile.STATE_PLAYING
//                || mShakeFile.getState() == ShakeFile.STATE_PAUSED) {
//            stopPlayback();
//        }
        mHandler.removeCallbacks(updateThread);
        playerService.unregisterPlayStateListener(mPlayStateListener);
        super.onStop();
    }
}
