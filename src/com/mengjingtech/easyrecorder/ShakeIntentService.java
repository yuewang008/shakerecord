package com.mengjingtech.easyrecorder;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.mengjingtech.easyrecorder.ShakeFileManager.FileItem;
import com.mengjingtech.lib.Util;

public class ShakeIntentService extends IntentService {
	private static final boolean DEBUG = ShakeConst.DEBUG;
	private static final String TAG = "ShakeIntentService";
	public static final String ACTION_SYNC_FILE_DB = "com.mengjingtech.easyrecorder.ACTION_SYNC_FILE_DB";
	public static final int SYNC_FILE_DB_BATCH_COUNT = 10; // every 10 items
															// sync and send
															// notify to app.

	private Messenger messenger = null;

	public ShakeIntentService() {
		super("ShakeIntentService");
	}

	@Override
	public IBinder onBind(Intent intent) {
		return super.onBind(intent);
	}

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void setIntentRedelivery(boolean enabled) {
		super.setIntentRedelivery(enabled);
		System.out.println("setIntentRedelivery");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		String action = intent.getAction();
		if (action.equals(ACTION_SYNC_FILE_DB)) {
			messenger = (Messenger) intent.getExtras().get(ShakeConst.EXTRA_MESSAGER);
			syncFileDBBatch(this);
		}
	}

	@Override
	public void onDestroy() {
		System.out.println("onDestroy");
		super.onDestroy();
	}

	private void notifySyncStatus(int what) {
		Message msg = Message.obtain();
		msg.what = what;
		if (messenger != null) {
			try {
				messenger.send(msg);
			} catch (Exception e) {
				Log.w(getClass().getName(),
						"Exception Message: " + e.toString());
			}
		}
	}

	public void syncFileDBBatch(Context context) {
		ShakeFileManager sfm = ShakeFileManager
				.getInstance(getApplicationContext());
		HashMap<String, FileItem> sfMap = sfm.getFileIDDB();

		// File dir = new File(Util.appSaveDir);
		// File[] subFiles = dir.listFiles();
		List<File> subFiles = Util.getFileSort(Util.appSaveDir);
		int nubmerOfFiles = subFiles.size();
		if (nubmerOfFiles > 0) {
			int fileCount = 0;
			for (File f : subFiles) {
				String fileName = f.getName();
				FileItem fileItem = sfMap.get(fileName);
				if (fileItem != null) {
					fileItem.exist = true;
				} else {
					ShakeFile sf = new ShakeFile(f);
					sfm.addFileToDB(sf);
				}
				fileCount++;

				if (fileCount == nubmerOfFiles) {
					// if all files are processed.
					notifySyncStatus(ShakeConst.ServiceMessage.SYNC_FILE_DB_DONE);
				} else if ((fileCount % SYNC_FILE_DB_BATCH_COUNT) == 0) {
					// if one batch of files are processed, notify the app.
					notifySyncStatus(ShakeConst.ServiceMessage.SYNC_FILE_DB_BATCH);
				}
			}
		} else {
			notifySyncStatus(ShakeConst.ServiceMessage.SYNC_FILE_DB_DONE);
		}

		Collection<FileItem> co = sfMap.values();
		Iterator<FileItem> it = co.iterator();
		while (it.hasNext()) {
			FileItem fileItem = (FileItem) it.next();
			if (!fileItem.exist) {
				sfm.deleteFromDBbyID(fileItem.id);
			}
		}
		sfm.syncFileDBDone();
	}
}
