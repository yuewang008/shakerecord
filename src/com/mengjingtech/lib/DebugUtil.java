package com.mengjingtech.lib;

public class DebugUtil {
    public static int getLineNumber(Exception e) {
        StackTraceElement[] trace = e.getStackTrace();
        if (trace == null || trace.length == 0)
            return -1;
        return trace[0].getLineNumber();
    }
    
    public static String getFileName(Exception e) {
        StackTraceElement[] trace = e.getStackTrace();
        if (trace == null || trace.length == 0)
            return "";
        return trace[0].getFileName();
    }
    
    public static String getMethodName(Exception e) {
        StackTraceElement[] trace = e.getStackTrace();
        if (trace == null || trace.length == 0)
            return "";
        return trace[0].getMethodName();
    }
    
    public static String getClassName(Exception e) {
        StackTraceElement[] trace = e.getStackTrace();
        if (trace == null || trace.length == 0)
            return "";
        return trace[0].getClassName();
    }
}
