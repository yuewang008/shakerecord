package com.mengjingtech.lib;

public class MengJingTechException extends Throwable{

	public MengJingTechException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MengJingTechException(String detailMessage, Throwable cause) {
		super(detailMessage, cause);
		// TODO Auto-generated constructor stub
	}

	public MengJingTechException(String detailMessage) {
		super(detailMessage);
		// TODO Auto-generated constructor stub
	}

	public MengJingTechException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

}
