package com.mengjingtech.lib;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.provider.ContactsContract;
import android.util.Log;

public class Util {
	static final String TAG = "Util";
	final static boolean DEBUG = true;

	private static final String COM_NAME = "mengjingtech";

	public static String comSaveDir = null;
	public static String appSaveDir = null;

	private static MediaPlayer mMediaPlayer = new MediaPlayer();

	public static String getStoragePath() {
		return appSaveDir;
	}

	public static boolean createDir(String dirPath) {
		File dir = new File(dirPath);
		if (!dir.exists()) {
			dir.mkdir();
		}
		if (!dir.exists()) {
			return false;
		}
		return true;
	}

	public static boolean initialFolders(String appName) {
		String state = Environment.getExternalStorageState();

		if (state.equals(Environment.MEDIA_MOUNTED)) {
			comSaveDir = Environment.getExternalStorageDirectory()
					+ File.separator + COM_NAME + File.separator;
			appSaveDir = comSaveDir + appName + File.separator;
			if (DEBUG)
				Log.d(TAG, "initialFolders + comSaveDir = " + comSaveDir
						+ " appSaveDir = " + appSaveDir);
			if (!createDir(comSaveDir))
				return false;
			if (!createDir(appSaveDir))
				return false;
		}
		return true;
	}

	public static String getFileName(String url) {
		String filename = null;
		if (url != null) {
			filename = url.substring(url.lastIndexOf("/") + 1);
		}
		return filename;
	}

	public static String getStandardTime(long timestamp) {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss",
				Locale.getDefault());
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+0"));
		Date date = new Date(timestamp * 1000);
		return sdf.format(date);
	}

	public static String getDateTimeNameByTime(long timestamp) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault());
		sdf.setTimeZone(TimeZone.getDefault());
		return sdf.format(new Date());
	}

	public static String getTimeNameByTime(long timestamp) {
		SimpleDateFormat sdf = new SimpleDateFormat("HHmmss",
				Locale.getDefault());
		sdf.setTimeZone(TimeZone.getDefault());
		return sdf.format(new Date());
	}

	public static String getTimeDateNameByTime(long timestamp) {
		SimpleDateFormat sdf = new SimpleDateFormat("HHmmss_yyyyMMdd",
				Locale.getDefault());
		sdf.setTimeZone(TimeZone.getDefault());
		return sdf.format(new Date());
	}

	public static String getLocaleDateTime(long timestamp) {
		Date date = new Date(timestamp);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
				Locale.getDefault());
		return sdf.format(date);
	}

	public static String getLocaleDate(long timestamp) {
		Date date = new Date(timestamp);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd",
				Locale.getDefault());
		return sdf.format(date);
	}

	/**
	 * @param time
	 * @param seperate
	 * @return the string represent the time time. if the time is later than
	 *         separate, return HH:mm, if the time is before today, return date.
	 */
	public static String getLocaleDateTime(long time, long seperate) {
		Date sepDate = new Date(seperate);
		Date date = new Date(time);
		SimpleDateFormat sdf;
		if (date.after(sepDate)) {
			sdf = new SimpleDateFormat("HH:mm", Locale.getDefault());
		} else {
			sdf = new SimpleDateFormat("MMM dd", Locale.getDefault());
		}
		return sdf.format(date);
	}

	public static Calendar getToday() {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(System.currentTimeMillis());
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		return cal;
	}

	public static boolean MoveFile(String oldName, String newName) {
		File sd = Environment.getExternalStorageDirectory();
		// File (or directory) to be moved
		String sourcePath = appSaveDir + oldName;
		File file = new File(sd, sourcePath);
		// Destination directory
		return file.renameTo(new File(sd, newName));
	}

	public static Date formatStringToTime(String sDate) {
		Calendar cal = Calendar.getInstance();

		Date date = null;
		if (sDate == null)
			return null;
		DateFormat df = new SimpleDateFormat("HH:mm");
		try {
			date = df.parse(sDate);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		if (DEBUG)
			Log.d(TAG, date.toString());
		cal.setTime(date);
		cal.set(1970, 0, 1);
		return cal.getTime();
	}

	public static String formatTimeToString(Date date) {
		if (date == null)
			return null;
		DateFormat df = new SimpleDateFormat("HH:mm");
		String sDate = df.format(date);
		if (DEBUG)
			Log.d(TAG, sDate);
		return sDate;
	}

	public static int getRecordTime(String path) {
		if (path == null || path.length() <= 5) {
			return 0;
		}
		int time = 0;
		try {
			mMediaPlayer.setDataSource(path);
			mMediaPlayer.prepare();
			long timeInMiniseconds = mMediaPlayer.getDuration();
			time = (int) Math.round(timeInMiniseconds / 1000.0);
			if (DEBUG)
				Log.d(TAG, "time = " + time);
			mMediaPlayer.reset();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return time;
	}

	public static String getContactDisplayNameByNumber(Context context,
			String number) {
		Uri uri = Uri.withAppendedPath(
				ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
				Uri.encode(number));
		String name = "";

		ContentResolver contentResolver = context.getContentResolver();
		Cursor contactLookup = contentResolver.query(uri,
				new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME },
				null, null, null);

		try {
			if (contactLookup != null && contactLookup.getCount() > 0) {
				contactLookup.moveToNext();
				name = contactLookup.getString(contactLookup
						.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
				// String contactId =
				// contactLookup.getString(contactLookup.getColumnIndex(BaseColumns._ID));
			}
		} finally {
			if (contactLookup != null) {
				contactLookup.close();
			}
		}

		return name;
	}

	public static String getContactNameFromPhoneBook(Context context,
			String phoneNum) {
		String contactName = "";
		String number = "";
		String preprocessNumber = preprocessCallNumber(phoneNum);
		if (phoneNum != null && phoneNum.trim().length() > 0) {
			ContentResolver cr = context.getContentResolver();
			Cursor pCur = cr.query(
					ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
					ContactsContract.CommonDataKinds.Phone.NUMBER + " = ? ",
					new String[] { preprocessNumber }, null);
			while (pCur.moveToFirst()) {
				number = pCur
						.getString(pCur
								.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
				if (getNumOnlyString(number).equals(phoneNum)) {
					contactName = pCur
							.getString(pCur
									.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
					break;
				}
			}
			pCur.close();
		}
		return contactName;
	}

	public static String getNumOnlyString(String input) {
		String str = input.trim();
		String str2 = "";
		if (str != null && !"".equals(str)) {
			for (int i = 0; i < str.length(); i++) {
				if (str.charAt(i) >= 48 && str.charAt(i) <= 57) {
					str2 += str.charAt(i);
				}
			}

		}
		return str2;
	}

	public static String preprocessCallNumber(String input) {
		if (input.startsWith("1")) {
			if (input.length() == 1) {
				return input;
			} else if (input.length() > 1 && input.length() < 5) {
				return input.substring(0, 1) + "-"
						+ input.substring(1, input.length());
			} else if (input.length() >= 5 && input.length() < 8) {
				return input.substring(0, 1) + "-" + input.substring(1, 4)
						+ "-" + input.substring(4, input.length());
			} else if (input.length() >= 8) {
				return input.substring(0, 1) + "-" + input.substring(1, 4)
						+ "-" + input.substring(4, 7) + "-"
						+ input.substring(7, input.length());
			}
		} else {
			if (input.length() <= 3) {
				return input;
			} else if (input.length() > 3 && input.length() <= 7) {
				return input.substring(0, 3) + "-"
						+ input.substring(3, input.length());
			} else if (input.length() >= 7) {
				return "(" + input.substring(0, 3) + ") "
						+ input.substring(3, 6) + "-"
						+ input.substring(6, input.length());
			}
		}
		return "";
	}

	/**
	 * 获取目录下所有文件(按时间排序)
	 * 
	 * @param path
	 * @return
	 */
	public static List<File> getFileSort(String path) {

		List<File> list = getFiles(path, new ArrayList<File>());

		if (list != null && list.size() > 0) {

			Collections.sort(list, new Comparator<File>() {
				public int compare(File file, File newFile) {
					if (file.lastModified() < newFile.lastModified()) {
						return 1;
					} else if (file.lastModified() == newFile.lastModified()) {
						return 0;
					} else {
						return -1;
					}

				}
			});

		}

		return list;
	}

	/**
	 * 
	 * 获取目录下所有文件
	 * 
	 * @param realpath
	 * @param files
	 * @return
	 */
	public static List<File> getFiles(String realpath, List<File> files) {

		File realFile = new File(realpath);
		if (realFile.isDirectory()) {
			File[] subfiles = realFile.listFiles();
			for (File file : subfiles) {
				if (file.isDirectory()) {
					getFiles(file.getAbsolutePath(), files);
				} else {
					files.add(file);
				}
			}
		}
		return files;
	}
}
